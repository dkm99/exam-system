package com.bxx.dao;

import com.bxx.pojo.School;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.util.List;

@Mapper
@Repository //持久层注解
public interface SchoolDao {


    /**
     * 多条件获取所有学校
     * @return
     */
    List<School> listSchools(PageUtil<School> pageUtil);
    /**
     * 多条件获取所有学校总条数
     * @return
     */
    int listSchoolsCount(PageUtil<School> pageUtil);

    /**
     * 添加学校
     * @param school
     * @return
     */
    Integer insertSchool(School school);

    /**
     * 修改学校
     * @param school
     * @return
     */
    Integer updateSchool(School school);

    /**
     * 删除学校
     * @param schoolId
     * @return
     */
    Integer deleteSchool(String schoolId);
}
