package com.bxx.dao;

import com.bxx.pojo.Major;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface MajorDao {
    /**
     * 多条件获取所有专业
     * @return
     */
    List<Major> listMajors(PageUtil<Major> pageUtil);
    /**
     * 多条件获取所有专业总条数
     * @return
     */
    int listMajorsCount(PageUtil<Major> pageUtil);

    /**
     * 添加专业
     * @param major
     * @return
     */
    Integer insertMajor(Major major);

    /**
     * 修改专业
     * @param major
     * @return
     */
    Integer updateMajor(Major major);

    /**
     * 删除专业
     * @param majorId
     * @return
     */
    Integer deleteMajor(String majorId);
}
