package com.bxx.dao;

import com.bxx.pojo.Class;
import com.bxx.pojo.User;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: ClassDao
 * Descriptioen:
 * date: 2022/2/13 17:31
 * author :wfl
 */

@Mapper
@Repository //持久层注解
public interface ClassDao {
    /**
     * 多条件获取所有班级
     * @return
     */
    List<Class> listClass(PageUtil<Class> pageUtil);
    /**
     * 多条件获取所有班级总条数
     * @return
     */
    int listClassCount(PageUtil<Class> pageUtil);

    /**
     * 通过id获取班级对象
     * @param classId
     * @return
     */
    Class getClassByClassId(String classId);
    /**
     * 添加班级
     * @param _class
     * @return
     */
    Integer insertClass(Class _class);

    /**
     * 修改班级
     * @param _class
     * @return
     */
    Integer updateClass(Class _class);

    /**
     * 删除班级
     * @param classId
     * @return
     */
    Integer deleteClass(String classId);

}
