package com.bxx.dao;

import com.bxx.pojo.ExamClass;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ExamClassDao {
    /**
     * 根据考试id获取拥有的班级
     *
     * @param examId
     * @return
     */
    List<ExamClass> getExamClassByExamId(String examId);

    /**
     * 根据考试id和班级id获取对象
     *
     * @param examClass
     * @return
     */
    ExamClass getExamClassByExamIdAndClassId(ExamClass examClass);


}