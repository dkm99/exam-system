package com.bxx.dao;

import com.bxx.pojo.Department;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface DepartmentDao {
    /**
     * 多条件获取所有院系
     * @return
     */
    List<Department> listDepartments(PageUtil<Department> pageUtil);
    /**
     * 多条件获取所有院系总条数
     * @return
     */
    int listDepartmentsCount(PageUtil<Department> pageUtil);

    /**
     * 添加院系
     * @param department
     * @return
     */
    Integer insertDepartment(Department department);

    /**
     * 修改院系
     * @param department
     * @return
     */
    Integer updateDepartment(Department department);

    /**
     * 删除院系
     * @param departmentId
     * @return
     */
    Integer deleteDepartment(String departmentId);
}
