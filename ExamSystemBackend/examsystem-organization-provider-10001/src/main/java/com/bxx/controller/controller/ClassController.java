package com.bxx.controller.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Select;
import com.bxx.pojo.User;
import com.bxx.service.ClassService;

import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/class")
public class ClassController {

	@Autowired
	private ClassService classService;

	@GetMapping("/listClass")
	public PageUtil<Class> listClass(@RequestBody PageUtil<Class> pageUtil) {

		return classService.listClass(pageUtil);
	}
	//通过考试id查询班级集合
	@GetMapping("/getExamIdlistClass")
	public PageUtil<Class> getExamIdlistClass(@RequestBody String examId){
		return  classService.getExamIdlistClass(examId);
	}
	//新增用户
	@PostMapping("/insertClass")
	public R insertUser(@RequestBody Class _class) {
		return classService.insertClass(_class);
	}
	//修改用户
	@PutMapping("/updateClass")
	public R updateUser(@RequestBody Class _class) {
		return classService.updateClass(_class);
	}
	//删除用户
	@DeleteMapping("/deleteClass")
	public R deleteUser(@RequestBody String classId) {
		return classService.deleteClass(classId);
	}

	

}
