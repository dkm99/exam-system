package com.bxx.controller.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Department;
import com.bxx.pojo.User;
import com.bxx.service.ClassService;

import com.bxx.service.DepartmentService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/listDepartments")
    public PageUtil<Department> listDepartments(@RequestBody PageUtil<Department> pageUtil) {
        return departmentService.listDepartments(pageUtil);
    }
    //新增院系
    @PostMapping("/insertDepartment")
    public R insertDepartment(@RequestBody Department department) {
        return departmentService.insertDepartment(department);
    }
    //修改院系
    @PutMapping("/updateDepartment")
    public R updateDepartment(@RequestBody Department department) {
        return departmentService.updateDepartment(department);
    }
    //删除院系
    @DeleteMapping("/deleteDepartment")
    public R deleteDepartment(@RequestBody String departmentId) {
        return departmentService.deleteDepartment(departmentId);
    }



}
