package com.bxx.controller.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.School;
import com.bxx.service.ClassService;
import com.bxx.service.SchoolService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/school")
public class SchoolController {

	@Autowired
	private SchoolService schoolService;

	@GetMapping("/listSchools")
	public PageUtil<School> listSchools(@RequestBody PageUtil<School> pageUtil) {
		return schoolService.listSchools(pageUtil);
	}
	//新增学校
	@PostMapping("/insertSchool")
	public R insertSchool(@RequestBody School school) {
		return schoolService.insertSchool(school);
	}
	//修改学校
	@PutMapping("/updateSchool")
	public R updateUser(@RequestBody School school) {
		return schoolService.updateSchool(school);
	}
	//删除学校
	@DeleteMapping("/deleteSchool")
	public R deleteSchool(@RequestBody String schoolId) {
		return schoolService.deleteSchool(schoolId);
	}

	

}
