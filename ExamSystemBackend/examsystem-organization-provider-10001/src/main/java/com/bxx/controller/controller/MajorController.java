package com.bxx.controller.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Major;
import com.bxx.service.ClassService;
import com.bxx.service.MajorService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/major")
public class MajorController {

	@Autowired
	private MajorService majorService;

	@GetMapping("/listMajors")
	public PageUtil<Major> listMajors(@RequestBody PageUtil<Major> pageUtil) {

		return majorService.listMajors(pageUtil);
	}
	//新增专业
	@PostMapping("/insertMajor")
	public R insertMajor(@RequestBody Major major) {
		return majorService.insertMajor(major);
	}
	//修改专业
	@PutMapping("/updateMajor")
	public R updateUser(@RequestBody Major major) {
		return majorService.updateMajor(major);
	}
	//删除专业
	@DeleteMapping("/deleteMajor")
	public R deleteMajor(@RequestBody String majorId) {
		return majorService.deleteMajor(majorId);
	}

	

}
