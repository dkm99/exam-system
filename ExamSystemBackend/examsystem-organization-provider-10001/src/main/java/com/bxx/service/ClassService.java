package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Select;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface ClassService {
    /**
     * 多条件获取所有班级
     * @return
     */
    PageUtil<Class> listClass(PageUtil<Class> pageUtil);

    /**
     * 通过考试id获取班级集合
     * @param examId
     * @return
     */
    PageUtil<Class>  getExamIdlistClass(String examId);

    /**
     * 添加班级
     * @param _class
     * @return
     */
    R insertClass(Class _class);

    /**
     * 修改班级
     * @param _class
     * @return
     */
    R updateClass(Class _class);

    /**
     * 删除班级
     * @param classId
     * @return
     */
    R deleteClass(String classId);
}
