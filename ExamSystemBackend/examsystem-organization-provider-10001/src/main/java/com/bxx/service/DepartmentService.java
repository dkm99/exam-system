package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Department;
import com.bxx.pojo.School;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface DepartmentService {
    /**
     * 多条件获取所有院系
     * @return
     */
    PageUtil<Department> listDepartments(PageUtil<Department> pageUtil);


    /**
     * 添加院系
     * @param department
     * @return
     */
    R insertDepartment(Department department);

    /**
     * 修改院系
     * @param department
     * @return
     */
    R updateDepartment(Department department);

    /**
     * 删除院系
     * @param departmentId
     * @return
     */
    R deleteDepartment(String departmentId);
}
