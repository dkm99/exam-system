package com.bxx.service.impl;

import com.bxx.dao.SchoolDao;
import com.bxx.pojo.School;
import com.bxx.service.SchoolService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: SchoolServiceImpl
 * Descriptioen:
 * date: 2022/2/13 18:26
 * author :wfl
 */

@Service
public class SchoolServiceImpl implements SchoolService {
    @Autowired
    private SchoolDao schoolDao;

    @Override
    public PageUtil<School> listSchools(PageUtil<School> pageUtil) {
        List<School> schools = schoolDao.listSchools(pageUtil);
        int listSchoolsCount= schoolDao.listSchoolsCount(pageUtil);
        pageUtil.setData(schools);
        pageUtil.setCount(listSchoolsCount);
        return pageUtil;
    }

    @Override
    public R insertSchool(School school) {
        school.setSchoolId(UUID.randomUUID().toString());
        school.setCreateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = schoolDao.insertSchool(school);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateSchool(School school) {

        Integer r = schoolDao.updateSchool(school);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteSchool(String schoolId) {
        Integer r = schoolDao.deleteSchool(schoolId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }
}
