package com.bxx.service.impl;

import com.bxx.dao.MajorDao;
import com.bxx.pojo.Class;
import com.bxx.pojo.Major;
import com.bxx.pojo.School;
import com.bxx.service.MajorService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.krb5.internal.PAData;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: MajorServiceImpl
 * Descriptioen:
 * date: 2022/2/13 20:31
 * author :wfl
 */

@Service
public class MajorServiceImpl implements MajorService {

    @Autowired
    private MajorDao majorDao;

    @Override
    public PageUtil<Major> listMajors(PageUtil<Major> pageUtil) {
        List<Major> majors = majorDao.listMajors(pageUtil);
        int listMajorsCount = majorDao.listMajorsCount(pageUtil);
        pageUtil.setData(majors);
        pageUtil.setCount(listMajorsCount);
        return pageUtil;
    }

    @Override
    public R insertMajor(Major major) {
        major.setMajorId(UUID.randomUUID().toString());
        major.setCreateTime(DateParse.parseTimeStamp(new Date()));
       Integer r = majorDao.insertMajor(major);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Override
    public R  updateMajor(Major major) {
        major.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = majorDao.updateMajor(major);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Override
    public R  deleteMajor(String majorId) {
        Integer r = majorDao.deleteMajor(majorId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }
}
