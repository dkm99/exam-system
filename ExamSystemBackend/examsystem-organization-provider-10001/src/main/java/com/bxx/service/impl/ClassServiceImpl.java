package com.bxx.service.impl;

import com.bxx.dao.ClassDao;
import com.bxx.dao.ExamClassDao;
import com.bxx.pojo.Class;
import com.bxx.pojo.ExamClass;
import com.bxx.pojo.Select;
import com.bxx.service.ClassService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: ClassServiceImpl
 * Descriptioen:
 * date: 2022/2/13 20:08
 * author :wfl
 */

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    public ClassDao classDao;
   @Autowired
    private ExamClassDao examClassDao;

    @Override
    public PageUtil<Class> listClass(PageUtil<Class> pageUtil) {
        List<Class> classes = classDao.listClass(pageUtil);
        int listClassCount = classDao.listClassCount(pageUtil);
        pageUtil.setData(classes);
        pageUtil.setCount(listClassCount);
        return pageUtil;

    }

    @Override
    public PageUtil<Class> getExamIdlistClass(String examId) {
        List<ExamClass> examClassByExamId = examClassDao.getExamClassByExamId(examId);

        List<Class> classes=new ArrayList<>();
        for(int i=0;i<examClassByExamId.size();i++){
            System.out.println("我是多对多=="+examClassByExamId.get(i).getClassId());
            String classId = examClassByExamId.get(i).getClassId();
            Class _class= classDao.getClassByClassId(classId);
            classes.add(_class);
        }
        System.out.println("classes的长度"+classes.size());
        PageUtil<Class> classPageUtil = new PageUtil<>();
        classPageUtil.setData(classes);

        return classPageUtil;
    }


    @Override
    public R insertClass(Class _class) {
        _class.setClassId(UUID.randomUUID().toString());
        _class.setCreateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = classDao.insertClass(_class);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateClass(Class _class) {
        _class.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = classDao.updateClass(_class);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteClass(String classId) {
        System.out.println("classid===="+classId);
        Integer r = classDao.deleteClass(classId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }
}
