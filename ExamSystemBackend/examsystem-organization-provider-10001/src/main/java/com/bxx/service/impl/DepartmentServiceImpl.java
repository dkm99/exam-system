package com.bxx.service.impl;

import com.bxx.dao.DepartmentDao;
import com.bxx.pojo.Class;
import com.bxx.pojo.Department;
import com.bxx.service.DepartmentService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: DepartmentServiceImpl
 * Descriptioen:
 * date: 2022/2/13 20:26
 * author :wfl
 */

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public PageUtil<Department> listDepartments(PageUtil<Department> pageUtil) {

        List<Department> departments = departmentDao.listDepartments(pageUtil);
        int listDepartmentsCount = departmentDao.listDepartmentsCount(pageUtil);
        pageUtil.setData(departments);
        pageUtil.setCount(listDepartmentsCount);
        return pageUtil;
    }

    @Transactional //开启事务
    @Override
    public R insertDepartment(Department department) {
        department.setDepartmentId(UUID.randomUUID().toString());
        department.setCreateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = departmentDao.insertDepartment(department);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateDepartment(Department department) {
        department.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = departmentDao.updateDepartment(department);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Override
    public R deleteDepartment(String departmentId) {
        Integer r = departmentDao.deleteDepartment(departmentId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }
}
