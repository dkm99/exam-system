package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Major;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface MajorService {
    /**
     * 多条件获取所有专业
     * @return  major
     */
    PageUtil<Major> listMajors(PageUtil<Major> pageUtil);


    /**
     * 添加专业
     * @param major
     * @return
     */
    R insertMajor(Major major);

    /**
     * 修改专业
     * @param major
     * @return
     */
    R updateMajor(Major major);

    /**
     * 删除专业
     * @param majorId
     * @return
     */
    R deleteMajor(String majorId);
}
