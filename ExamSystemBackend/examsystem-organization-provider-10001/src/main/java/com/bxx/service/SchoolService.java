package com.bxx.service;

import com.alibaba.druid.sql.PagerUtils;
import com.bxx.pojo.Class;
import com.bxx.pojo.School;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

import java.util.List;

/**
 * ClassName: SchoolService
 * Descriptioen:
 * date: 2022/2/13 18:22
 * author :wfl
 */


public interface SchoolService {

    /**
     * 多条件获取所有学校
     * @return
     */
    PageUtil<School> listSchools(PageUtil<School> pageUtil);


    /**
     * 添加学校
     * @param school
     * @return
     */
    R insertSchool(School school);

    /**
     * 修改学校
     * @param school
     * @return
     */
    R updateSchool(School school);

    /**
     * 删除学校
     * @param schoolId
     * @return
     */
    R deleteSchool(String schoolId);
}
