package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
//import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
//在微服务启动的时候就能去加载我们自定义Ribbon类
//@RibbonClient(name = "EXAMSYSTEM-TEST-PROVIDER-8001")
@EnableFeignClients(basePackages = {"com.bxx.service"})
public class TestConsumer_8002 {
    public static void main(String[] args) {
        SpringApplication.run(TestConsumer_8002.class,args);
    }
}
