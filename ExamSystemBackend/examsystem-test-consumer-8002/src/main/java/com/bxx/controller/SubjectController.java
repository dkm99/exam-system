package com.bxx.controller;

import com.bxx.pojo.Subject;
import com.bxx.service.SubjectClientService;
import com.bxx.util.JsonUtil;
import com.bxx.util.LayuiJsonUtil;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/subject")
public class SubjectController {


    @Autowired
    private SubjectClientService subjectService;

    @GetMapping("/list")
    public PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil){
        PageUtil<Subject> subjectPageUtil = this.subjectService.listSubjects(pageUtil);
        return subjectPageUtil;
    }

//    @GetMapping("/get/{subjectId}")
//    public R getSubject(@PathVariable("subjectId") String subjectId){
//        return this.subjectService.getSubject(subjectId);
//    }


    @PostMapping("/insertSubject")
    public R insertSubject(Subject subject){
        return this.subjectService.insertSubject(subject);
    }

    @DeleteMapping("/deleteSubject")
    public R deleteSubject(String subjectId){

        return this.subjectService.deleteSubject(subjectId);
    }

    @PutMapping("/updateSubject")
    public R updateSubject(Subject subject){
        return this.subjectService.updateSubject(subject);
    }


    //==============================

    //消费者不应该有service层
    //RestTemplate... 供我们调用就可以！注册到spring中

    //提供多种便捷访问远程http服务的方法，简单的restful服务模块~
//    @Autowired
//    private RestTemplate restTemplate;
//
//    private static final String REST_URL_PREFIX="http://EXAMSYSTEM-TEST-PROVIDER-8001";
//
//    @GetMapping("/list")
//    public LayuiJsonUtil listSubjects(){
//        return restTemplate.getForObject(REST_URL_PREFIX+"/subject/list", LayuiJsonUtil.class);
//    }
//
//    @GetMapping("/get/{subjectId}")
//    public JsonUtil getSubject(@PathVariable("subjectId") String subjectId){
//        return restTemplate.getForObject(REST_URL_PREFIX+"/subject/get/"+subjectId, JsonUtil.class);
//    }
//
//
//    @PostMapping("/insertSubject")
//    public JsonUtil insertSubject(Subject subject){
//        return restTemplate.postForObject(REST_URL_PREFIX+"/subject/insertSubject",subject,JsonUtil.class);
//    }
//
//    @DeleteMapping("/deleteSubject")
//    public ResponseEntity<JsonUtil> deleteSubject(String subjectId){
//        ResponseEntity<JsonUtil> exchange = restTemplate.exchange(REST_URL_PREFIX + "/subject/deleteSubject", HttpMethod.DELETE, new HttpEntity<String>(subjectId), JsonUtil.class);
//        return exchange;
//    }
//
//    @PutMapping("/updateSubject")
//    public JsonUtil updateSubject(Subject subject){
//        System.out.println("consumer.updateSubject.subject>"+subject.toString());
//        //设置Http的Header
//        //import package: import org.springframework.http.HttpHeaders;
//        //HttpHeaders headers = new HttpHeaders();
//        //headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
//
//        //设置访问参数
//        Map<String, Object> params = new HashMap<>();
//        //params.put("subject", subject);
//        params.put("subjectId", "test1");
//        params.put("subjectName", "test1");
//        params.put("subjectProfile", "test1");
//
//        //params.put("")
//
////        MultiValueMap<String,Object> paramMap=new LinkedMultiValueMap<>();
////        paramMap.add("subject",subject);
////        paramMap.add("test","123test");
//        //设置访问的Entity
//        HttpEntity<Map> entity = new HttpEntity<>(params);
//
//        ResponseEntity<JsonUtil> exchange = restTemplate.exchange(REST_URL_PREFIX + "/subject/updateSubject", HttpMethod.PUT, entity, JsonUtil.class);
//        return exchange.getBody();
//    }
}
