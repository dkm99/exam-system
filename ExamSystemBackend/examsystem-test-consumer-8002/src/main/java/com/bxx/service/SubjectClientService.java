package com.bxx.service;

import com.bxx.pojo.Subject;
import com.bxx.util.JsonUtil;
import com.bxx.util.LayuiJsonUtil;
//import org.springframework.cloud.openfeign.FeignClient;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-TEST-PROVIDER")
@RequestMapping("/subject")
public interface SubjectClientService {

    /**
     * 获取全部科目
     * @return 所有科目集合
     */
    @GetMapping("/list")
    PageUtil<Subject> listSubjects(@RequestParam(value = "pageUtil",required = false) PageUtil<Subject> pageUtil);

    /**
     * 根据id获取单个科目
     * @param subjectId
     * @return
     */
    //@GetMapping("/get/{subjectId}")
    //JsonUtil getSubject(@PathVariable("subjectId") String subjectId);

    /**
     * 新增科目
     * @param subject 新科目信息
     * @return
     */
    @PostMapping("/insertSubject")
    R insertSubject(Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    @DeleteMapping("/deleteSubject")
    R deleteSubject(String subjectId);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    @PutMapping("/updateSubject")
    R updateSubject(Subject subject);
}
