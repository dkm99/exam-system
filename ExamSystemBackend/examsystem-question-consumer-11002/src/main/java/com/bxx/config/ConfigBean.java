package com.bxx.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBean {//@Configuration -- spring中的 applicationContext.xml

    //配置负载均衡实现RestTemplate
//    @Bean
//    @LoadBalanced   //Ribbon
//    public RestTemplate getRestTemplate(){
//        return new RestTemplate();
//    }

    //IRule
    //RoundRobinRule：轮询
    @Bean
    public IRule getRule(){
        return new RoundRobinRule();
    }
}
