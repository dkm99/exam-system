package com.bxx.service;

import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.util.PageUtil;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(value = "EXAMSYSTEM-QUESTION-PROVIDER")
@RequestMapping("/questionType")
public interface QuestionTypeClientService {



    /**
     * 多条件获取所有题目类型
     * @return
     */
    @GetMapping("/listQuestionTypes")
    PageUtil<QuestionType> listQuestionTypes(@RequestBody PageUtil<QuestionType> pageUtil);


}
