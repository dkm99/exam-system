package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Question;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-QUESTION-PROVIDER")
@RequestMapping("/question")
public interface QuestionClientService {



    /**
     * 多条件获取所有题目
     * @return
     */
    @GetMapping("/listQuestions")
    PageUtil<Question> listQuestions(@RequestBody PageUtil<Question> pageUtil);
    /**
     * 添加试题
     * @param question
     * @return
     */
    @PostMapping("/insertQuestion")
    R insertQuestion(@RequestBody Question question);

    /**
     * 修改试题
     * @param question
     * @return
     */
    @PutMapping("/updateQuestion")
    R updateQuestion(@RequestBody Question question);

    /**
     * 删除试题
     * @param questionId
     * @return
     */
    @DeleteMapping("/deleteQuestion")
    R deleteQuestion(@RequestBody String questionId);

}
