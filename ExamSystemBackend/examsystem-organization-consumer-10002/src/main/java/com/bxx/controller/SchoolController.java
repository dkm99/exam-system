package com.bxx.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.School;
import com.bxx.service.ClassClientService;
import com.bxx.service.SchoolClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/school")
public class SchoolController {

    @Autowired
    private SchoolClientService schoolClientService;

    @GetMapping("/listSchools")
    public PageUtil<School> listSchools(PageUtil<School> pageUtil, School school, String beginDate, String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (school != null){
            map.put("school", school);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return schoolClientService.listSchools(pageUtil);
    }

    //新增学校
    @PostMapping("/insertSchool")
    public R insertSchool(School school) {
        return schoolClientService.insertSchool(school);
    }
    //修改学校
    @PutMapping("/updateSchool")
    public R updateSchool(School school) {
        System.out.println("consumer.updateSchool>"+school);
        return schoolClientService.updateSchool(school);
    }
    //删除学校
    @DeleteMapping("/deleteSchool")
    public R deleteSchool(String schoolId) {
        return schoolClientService.deleteSchool(schoolId);
    }


}
