package com.bxx.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Department;
import com.bxx.service.ClassClientService;
import com.bxx.service.DepartmentClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/department")
public class DepartmentController {

    @Autowired
    private DepartmentClientService departmentClientService;

    @GetMapping("/listDepartments")
    public PageUtil<Department> listDepartments(PageUtil<Department> pageUtil, Department department, String beginDate, String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (department != null){
            map.put("department", department);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return departmentClientService.listDepartments(pageUtil);
    }

    //新增院系
    @PostMapping("/insertDepartment")
    public R insertDepartment(Department department) {
        return departmentClientService.insertDepartment(department);
    }
    //修改院系
    @PutMapping("/updateDepartment")
    public R updateDepartment(Department department) {
        System.out.println("consumer.updateDepartment>"+department);
        return departmentClientService.updateDepartment(department);
    }
    //删除院系
    @DeleteMapping("/deleteDepartment")
    public R deleteDepartment(String departmentId) {
        return departmentClientService.deleteDepartment(departmentId);
    }


}
