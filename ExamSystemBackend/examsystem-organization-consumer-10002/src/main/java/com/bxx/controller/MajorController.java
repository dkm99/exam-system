package com.bxx.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Major;
import com.bxx.service.ClassClientService;
import com.bxx.service.MajorClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/major")
public class MajorController {

    @Autowired
    private MajorClientService majorClientService;

    @GetMapping("/listMajors")
    public PageUtil<Major> listMajors(PageUtil<Major> pageUtil, Major major, String beginDate, String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (major != null){
            map.put("major", major);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return majorClientService.listMajors(pageUtil);
    }

    //新增专业
    @PostMapping("/insertMajor")
    public R insertMajor(Major major) {
        return majorClientService.insertMajor(major);
    }
    //修改专业
    @PutMapping("/updateMajor")
    public R updateMajor(Major major) {
        System.out.println("consumer.updateMajor>"+major);
        return majorClientService.updateMajor(major);
    }
    //删除专业
    @DeleteMapping("/deleteMajor")
    public R deleteMajor(String majorId) {
        return majorClientService.deleteMajor(majorId);
    }


}
