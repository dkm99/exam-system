package com.bxx.controller;

import com.bxx.pojo.Class;
import com.bxx.pojo.Select;
import com.bxx.pojo.User;
import com.bxx.service.ClassClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/class")
public class ClassController {

    @Autowired
    private ClassClientService classClientService;

    @GetMapping("/listClass")
    public PageUtil<Class> listClass(PageUtil<Class> pageUtil,Class _class,String beginDate,String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (_class != null){
            map.put("_class", _class);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return classClientService.listClass(pageUtil);
    }
    //通过考试id查询班级集合
    @GetMapping("/getExamIdlistClass")
    public PageUtil<Class> getExamIdlistClass(String examId){
        return  classClientService.getExamIdlistClass(examId);
    }
    //新增班级
    @PostMapping("/insertClass")
    public R insertUser(Class _class) {
        return classClientService.insertClass(_class);
    }
    //修改班级
    @PutMapping("/updateClass")
    public R updateClass(Class _class) {
        System.out.println("consumer.updateClass>"+_class);
        return classClientService.updateClass(_class);
    }
    //删除班级
    @DeleteMapping("/deleteClass")
    public R deleteClass(String classId) {
        return classClientService.deleteClass(classId);
    }


}
