package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * ClassName: Select
 * Descriptioen:
 * date: 2022/4/8 20:00
 * author :wfl
 */

//专业
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Select implements Serializable {
private String name;
private Object value;
private Boolean selected=false;
}
