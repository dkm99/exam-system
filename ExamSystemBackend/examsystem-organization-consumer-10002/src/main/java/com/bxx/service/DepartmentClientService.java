package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Department;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-ORGANIZATION-PROVIDER")
@RequestMapping("/department")
public interface DepartmentClientService {



    /**
     * 多条件获取所有院系
     * @return
     */
    @GetMapping("/listDepartments")
    PageUtil<Department> listDepartments(@RequestBody PageUtil<Department> pageUtil);

    /**
     * 添加院系
     * @param department
     * @return
     */
    @PostMapping("/insertDepartment")
    R insertDepartment(@RequestBody Department department);

    /**
     * 修改院系
     * @param department
     * @return
     */
    @PutMapping("/updateDepartment")
    R updateDepartment(@RequestBody Department department);

    /**
     * 删除院系
     * @param departmentId
     * @return
     */
    @DeleteMapping("/deleteDepartment")
    R deleteDepartment(@RequestBody String departmentId);
}
