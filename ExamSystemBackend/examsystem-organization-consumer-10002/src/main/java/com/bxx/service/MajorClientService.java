package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.Major;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-ORGANIZATION-PROVIDER")
@RequestMapping("/major")
public interface MajorClientService {



    /**
     * 多条件获取所有专业
     * @return
     */
    @GetMapping("/listMajors")
    PageUtil<Major> listMajors(@RequestBody PageUtil<Major> pageUtil);

    /**
     * 添加专业
     * @param major
     * @return
     */
    @PostMapping("/insertMajor")
    R insertMajor(@RequestBody Major major);

    /**
     * 修改专业
     * @param major
     * @return
     */
    @PutMapping("/updateMajor")
    R updateMajor(@RequestBody Major major);

    /**
     * 删除专业
     * @param majorId
     * @return
     */
    @DeleteMapping("/deleteMajor")
    R deleteMajor(@RequestBody String majorId);
}
