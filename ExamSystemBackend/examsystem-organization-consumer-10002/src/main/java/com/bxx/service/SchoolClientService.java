package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.School;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-ORGANIZATION-PROVIDER")
@RequestMapping("/school")
public interface SchoolClientService {



    /**
     * 多条件获取所有学校
     * @return
     */
    @GetMapping("/listSchools")
    PageUtil<School> listSchools(@RequestBody PageUtil<School> pageUtil);

    /**
     * 添加学校
     * @param school
     * @return
     */
    @PostMapping("/insertSchool")
    R insertSchool(@RequestBody School school);

    /**
     * 修改学校
     * @param school
     * @return
     */
    @PutMapping("/updateSchool")
    R updateSchool(@RequestBody School school);

    /**
     * 删除学校
     * @param schoolId
     * @return
     */
    @DeleteMapping("/deleteSchool")
    R deleteSchool(@RequestBody String schoolId);
}
