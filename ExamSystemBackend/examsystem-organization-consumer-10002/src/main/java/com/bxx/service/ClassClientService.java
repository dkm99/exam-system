package com.bxx.service;

import com.bxx.pojo.Class;
import com.bxx.pojo.LoginDTO;
import com.bxx.pojo.Select;
import com.bxx.pojo.User;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-ORGANIZATION-PROVIDER")
@RequestMapping("/class")
public interface ClassClientService {



    /**
     * 多条件获取所有班级
     * @return
     */
    @GetMapping("/listClass")
    PageUtil<Class> listClass(@RequestBody PageUtil<Class> pageUtil);

    /**
     * 通过考试id获取班级集合
     * @param examId
     * @return
     */
    @GetMapping("/getExamIdlistClass")
     PageUtil<Class> getExamIdlistClass(@RequestBody String examId);

    /**
     * 添加班级
     * @param _class
     * @return
     */
    @PostMapping("/insertClass")
    R insertClass(@RequestBody Class _class);

    /**
     * 修改班级
     * @param _class
     * @return
     */
    @PutMapping("/updateClass")
    R updateClass(@RequestBody Class _class);

    /**
     * 删除用户
     * @param classId
     * @return
     */
    @DeleteMapping("/deleteClass")
    R deleteClass(@RequestBody String classId);
}
