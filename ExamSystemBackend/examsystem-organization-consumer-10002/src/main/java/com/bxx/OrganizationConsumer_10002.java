package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.bxx.service"})
public class OrganizationConsumer_10002 {

    public static void main(String[] args) {
        SpringApplication.run(OrganizationConsumer_10002.class,args);
    }
}
