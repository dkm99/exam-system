package com.bxx.controller;

import com.bxx.pojo.ExamRoom;
import com.bxx.service.ExamRoomClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/examRoom")
public class ExamRoomController {

    @Autowired
    private ExamRoomClientService examRoomClientService;

    @GetMapping("/listExamRooms")
    public PageUtil<ExamRoom> listExamRooms(PageUtil<ExamRoom> pageUtil, ExamRoom examRoom) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (examRoom != null){
            map.put("examRoom", examRoom);
        }

        pageUtil.setMap(map);
        return examRoomClientService.listExamRooms(pageUtil);
    }
   //新增考场
    @PostMapping("/insertExamRoom")
    public R insertExamRoom(ExamRoom examRoom) {
        return examRoomClientService.insertExamRoom(examRoom);
    }
    //修改考场
    @PutMapping("/updateExamRoom")
    public R updateExamRoom(ExamRoom examRoom) {

        return examRoomClientService.updateExamRoom(examRoom);
    }
    //删除考场
    @DeleteMapping("/deleteExamRoom")
    public R deleteExamRoom(String examRoomId) {
        return examRoomClientService.deleteExamRoom(examRoomId);
    }


}
