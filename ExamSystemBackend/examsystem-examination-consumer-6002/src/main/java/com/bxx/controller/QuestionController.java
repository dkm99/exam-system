package com.bxx.controller;

import com.bxx.pojo.Question;
import com.bxx.service.QuestionClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/question")
public class QuestionController {

    @Autowired
    private QuestionClientService questionClientService;

    @GetMapping("/listQuestions")
    public PageUtil<Question> listQuestions(PageUtil<Question> pageUtil, Question question, String beginDate, String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (question != null){
            map.put("question", question);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return questionClientService.listQuestions(pageUtil);
    }
   //新增试题
    @PostMapping("/insertQuestion")
    public R insertQuestion(Question _class) {
        return questionClientService.insertQuestion(_class);
    }
    //修改试题
    @PutMapping("/updateQuestion")
    public R updateQuestion(Question question) {
        System.out.println("consumer.updateQuestion>"+question);
        return questionClientService.updateQuestion(question);
    }
    //删除试题
    @DeleteMapping("/deleteQuestion")
    public R deleteQuestion(String questionId) {

        return questionClientService.deleteQuestion(questionId);
    }

     //通过科目和试题类型名称获取条数
    @PostMapping("/getSubjectandNameCount")
    public  R getSubjectandNameCount( String subjectId, String questionTypeName){
        System.out.println("sub"+subjectId);
        System.out.println("ques"+questionTypeName);
         return questionClientService.getSubjectandNameCount(subjectId, questionTypeName);
    }
}
