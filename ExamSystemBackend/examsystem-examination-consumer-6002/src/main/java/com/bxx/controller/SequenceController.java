package com.bxx.controller;

import com.bxx.pojo.Sequence;
import com.bxx.service.SequenceClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/sequence")
public class SequenceController {

    @Autowired
    private SequenceClientService sequenceClientService;

    @GetMapping("/listSequences")
    public PageUtil<Sequence> listSequences(PageUtil<Sequence> pageUtil, Sequence sequence) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (sequence != null){
            map.put("sequence", sequence);
        }

        pageUtil.setMap(map);
        return sequenceClientService.listSequences(pageUtil);
    }
   //新增批次
    @PostMapping("/insertSequence")
    public R insertSequence(Sequence sequence) {
        return sequenceClientService.insertSequence(sequence);
    }
    //修改批次
    @PutMapping("/updateSequence")
    public R updateSequence(Sequence sequence) {

        return sequenceClientService.updateSequence(sequence);
    }
    //删除批次
    @DeleteMapping("/deleteSequence")
    public R deleteSequence(String sequenceId) {
        return sequenceClientService.deleteSequence(sequenceId);
    }


}
