package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.service.ExamClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/exam")
public class ExamController {

    @Autowired
    private ExamClientService examClientService;

    @GetMapping("/listExams")
    public PageUtil<Exam> listExamRooms(PageUtil<Exam> pageUtil, Exam exam) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (exam != null){
            map.put("exam", exam);
        }

        pageUtil.setMap(map);
        return examClientService.listExams(pageUtil);
    }
   //新增考试
    @PostMapping("/insertExam")
    public R insertExam(Exam exam,String classIds) {
        return examClientService.insertExam(exam,classIds);
    }
    //修改考试
    @PutMapping("/updateExam")
    public R updateExam(Exam exam) {

        return examClientService.updateExam(exam);
    }
    //删除考试
    @DeleteMapping("/cancelExam")
    public R cancelExam(String examId) {
        return examClientService.cancelExam(examId);
    }


}
