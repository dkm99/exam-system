package com.bxx.controller;

import com.bxx.pojo.ExamType;
import com.bxx.service.ExamTypeClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/examType")
public class ExamTypeController {

    @Autowired
    private ExamTypeClientService examTypeClientService;

    @GetMapping("/listExamTypes")
    public PageUtil<ExamType> listExamTypes(PageUtil<ExamType> pageUtil, ExamType examType) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (examType != null){
            map.put("examType", examType);
        }

        pageUtil.setMap(map);
        return examTypeClientService.listExamTypes(pageUtil);
    }
   //新增考试类型
    @PostMapping("/insertExamType")
    public R insertExamType(ExamType examType) {
        return examTypeClientService.insertExamType(examType);
    }
    //修改考试类型
    @PutMapping("/updateExamType")
    public R updateExamType(ExamType examType) {

        return examTypeClientService.updateExamType(examType);
    }
    //删除考试类型
    @DeleteMapping("/deleteExamType")
    public R deleteExamType(String examTypeId) {
        return examTypeClientService.deleteExamType(examTypeId);
    }


}
