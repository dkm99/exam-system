package com.bxx.controller;

import com.bxx.pojo.QuestionType;
import com.bxx.service.QuestionTypeClientService;
import com.bxx.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/questionType")
public class QuestionTypeController {

    @Autowired
    private QuestionTypeClientService questionTypeClientService;

    @GetMapping("/listQuestionTypes")
    public PageUtil<QuestionType> listQuestions(PageUtil<QuestionType> pageUtil, QuestionType questionType, String beginDate, String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (questionType != null){
            map.put("questionType", questionType);
        }

        pageUtil.setMap(map);
        return questionTypeClientService.listQuestionTypes(pageUtil);
    }

}
