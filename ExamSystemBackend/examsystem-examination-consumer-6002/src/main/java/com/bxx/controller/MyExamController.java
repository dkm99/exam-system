package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.pojo.ExamDTO;
import com.bxx.service.MyExamClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/consumer/myExam")
public class MyExamController {

    @Autowired
    private MyExamClientService myExamClientService;


    @GetMapping("/getExamsByUserId")
    public PageUtil<Exam> getExamsByUserId(String userId){

        return myExamClientService.getExamsByUserId(userId);
    }

    //开始考试
    @GetMapping("/startExam")
    public R startExam(String examId, String userId){
        return myExamClientService.startExam(examId,userId);
    }

    //获取试题
    @GetMapping("/getTestQuestions")
    public R getTestQuestions(String examId){
        return myExamClientService.getTestQuestions(examId);
    }

    /**
     * 提交考试
     * @return
     */
//    @PostMapping("/submitExam")
//    public R submitExam(JSONObject jsonObject){
//        List<AnswerDTO> answerDTOs = jsonObject.getJSONArray("answerDTOs").toJavaList(AnswerDTO.class);
//        //System.out.println("examId>" + examId);
//        System.out.println("answerDTO>" + answerDTOs.toString());
//        return R.ok("数据接收成功");
//
//    }
//    @PostMapping("/submitExam")
//    public R submitExam(@RequestParam("examId") String examId,@RequestBody AnswerDTO[] answerDTOs){
//        System.out.println("examId>" + examId);
//        System.out.println("answerDTO>" + answerDTOs.toString());
//        return R.ok("数据接收成功");
//
//    }
    @PostMapping("/submitExam")
    public R submitExam(@RequestBody ExamDTO examDTO){
        System.out.println("examDTO>" + examDTO.toString());
        //System.out.println("answerDTO>" + answerDTOs.toString());
        return myExamClientService.submitExam(examDTO);

    }
}
