package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.pojo.Score;
import com.bxx.service.ExamClientService;
import com.bxx.service.ScoreClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/score")
public class ScoreController {

    @Autowired
    private ScoreClientService scoreClientService;

    @GetMapping("/listScores")
    public PageUtil<Score> listScores(PageUtil<Score> pageUtil, Score score) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (score != null){
            map.put("score", score);
        }

        pageUtil.setMap(map);
        return scoreClientService.listScores(pageUtil);
    }
    @GetMapping("/Rewinding")
    public R Rewinding( Score score){
        return scoreClientService.Rewinding(score);
    }

}
