package com.bxx.controller;

import com.bxx.pojo.Subject;
import com.bxx.service.SubjectClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/subject")
public class SubjectController {

    @Autowired
    private SubjectClientService subjectClientService;

    @GetMapping("/listSubjects")
    public PageUtil<Subject> listQuestions(PageUtil<Subject> pageUtil, Subject subject) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (subject != null){
            map.put("subject", subject);
        }

        pageUtil.setMap(map);
        return subjectClientService.listSubjects(pageUtil);
    }
   //新增科目
    @PostMapping("/insertSubject")
    public R insertSubject(Subject subject) {
        return subjectClientService.insertSubject(subject);
    }
    //修改科目
    @PutMapping("/updateSubject")
    public R updateSubject(Subject subject) {
        System.out.println("consumer.updateSubject>"+subject);
        return subjectClientService.updateSubject(subject);
    }
    //删除科目
    @DeleteMapping("/deleteSubject")
    public R deleteSubject(String subjectId) {
        return subjectClientService.deleteSubject(subjectId);
    }

    // 歌曲上传
    @RequestMapping("/upload")
    public Map<String, Object> upload(MultipartFile file) {

        return subjectClientService.upload(file);

    }
}
