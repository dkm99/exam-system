package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.pojo.RedisVTO;
import com.bxx.pojo.Sequence;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.service.ExamClientService;
import com.bxx.service.StudentExamAnswerClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/consumer/studentExamAnswer")
public class StudentExamAnswerController {

    @Autowired
    private StudentExamAnswerClientService studentExamAnswerClientService;

    @GetMapping("/listStudentExamAnswers")
    public PageUtil<StudentExamAnswer> listStudentExamAnswers(PageUtil<StudentExamAnswer> pageUtil, StudentExamAnswer studentExamAnswer) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (studentExamAnswer != null){
            map.put("studentExamAnswer", studentExamAnswer);
        }

        pageUtil.setMap(map);
        return studentExamAnswerClientService.listStudentExamAnswers(pageUtil);
    }

    //修改批次
    @PutMapping("/updateStudentExamAnswer")
    public R updateStudentExamAnswer(StudentExamAnswer studentExamAnswer) {

        return studentExamAnswerClientService.updateStudentExamAnswer(studentExamAnswer);
    }
    @GetMapping("/test")
    List<PageUtil<StudentExamAnswer>> test( PageUtil<StudentExamAnswer> pageUtil,String studentId){
       Map<String,Object> map = new HashMap<String, Object>();
        if (studentId != null){
            map.put("studentId", studentId);
        }

        pageUtil.setMap(map);

        return  studentExamAnswerClientService.test(pageUtil);
    }
    @PostMapping("/addStudentExamAnswerToRedis")
    public R addStudentExamAnswerToRedis( RedisVTO redisVTO){
        System.out.println("sad"+redisVTO.getTextPaperId());
        System.out.println("ss"+redisVTO.getScore());
        System.out.println("ss"+redisVTO.getStudentExamAnswerId());
        return  studentExamAnswerClientService.addStudentExamAnswerToRedis(redisVTO);

    }
    /**
     * 自动批改客观题
     * @param pageUtil
     * @return
     */
    @PostMapping("/automaticMarkingOfObjectiveQuestions")
    public R automaticMarkingOfObjectiveQuestions( PageUtil<StudentExamAnswer> pageUtil,String studentId,String textPaperId){
        Map<String,Object> map = new HashMap<String, Object>();
        if (studentId != null){
            map.put("studentId", studentId);
        }
        if (textPaperId != null){
            map.put("textPaperId", textPaperId);
        }

        pageUtil.setMap(map);

        return  studentExamAnswerClientService.AutomaticMarkingOfObjectiveQuestions(pageUtil);

    }
}
