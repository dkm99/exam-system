package com.bxx.service;

import com.bxx.pojo.ExamType;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/examType")
public interface ExamTypeClientService {



    /**
     * 多条件获取所有考试类型
     * @return
     */
    @GetMapping("/listExamTypes")
    PageUtil<ExamType> listExamTypes(@RequestBody PageUtil<ExamType> pageUtil);
    /**
     * 添加考试类型
     * @param examType
     * @return
     */
    @PostMapping("/insertExamType")
    R insertExamType(@RequestBody ExamType examType);

    /**
     * 修改考试类型
     * @param examType
     * @return
     */
    @PutMapping("/updateExamType")
    R updateExamType(@RequestBody ExamType examType);


    /**
     * 删除考试类型
     * @param examTypeId
     * @return
     */
    @DeleteMapping("/deleteExamType")
    R deleteExamType(@RequestBody String examTypeId);


}
