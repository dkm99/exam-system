package com.bxx.service;

import com.bxx.pojo.ExamRoom;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/examRoom")
public interface ExamRoomClientService {



    /**
     * 多条件获取所有考场
     * @return
     */
    @GetMapping("/listExamRooms")
    PageUtil<ExamRoom> listExamRooms(@RequestBody PageUtil<ExamRoom> pageUtil);
    /**
     * 添加考场
     * @param examRoom
     * @return
     */
    @PostMapping("/insertExamRoom")
    R insertExamRoom(@RequestBody ExamRoom examRoom);

    /**
     * 修改考场
     * @param examRoom
     * @return
     */
    @PutMapping("/updateExamRoom")
    R updateExamRoom(@RequestBody ExamRoom examRoom);

    /**
     * 删除考场
     * @param examRoomId
     * @return
     */
    @DeleteMapping("/deleteExamRoom")
    R deleteExamRoom(@RequestBody String examRoomId);


}
