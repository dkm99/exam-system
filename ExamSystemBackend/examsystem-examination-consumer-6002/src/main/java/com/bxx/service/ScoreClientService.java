package com.bxx.service;

import com.bxx.pojo.Exam;
import com.bxx.pojo.Score;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/score")
public interface ScoreClientService {



    /**
     * 多条件获取所有考试成绩
     * @return
     */
    @GetMapping("/listScores")
    PageUtil<Score> listScores(@RequestBody PageUtil<Score> pageUtil);

    /**
     * 改卷
     * @param score
     * @return
     */
    @GetMapping("/Rewinding")
    public R Rewinding(@RequestBody Score score);


}
