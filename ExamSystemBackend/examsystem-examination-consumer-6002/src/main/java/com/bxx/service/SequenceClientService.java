package com.bxx.service;

import com.bxx.pojo.Sequence;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/sequence")
public interface SequenceClientService {



    /**
     * 多条件获取所有批次
     * @return
     */
    @GetMapping("/listSequences")
    PageUtil<Sequence> listSequences(@RequestBody PageUtil<Sequence> pageUtil);
    /**
     * 添加批次
     * @param sequence
     * @return
     */
    @PostMapping("/insertSequence")
    R insertSequence(@RequestBody Sequence sequence);

    /**
     * 修改批次
     * @param sequence
     * @return
     */
    @PutMapping("/updateSequence")
    R updateSequence(@RequestBody Sequence sequence);

    /**
     * 删除批次
     * @param sequenceId
     * @return
     */
    @DeleteMapping("/deleteSequence")
    R deleteSequence(@RequestBody String sequenceId);


}
