package com.bxx.service;

import ch.qos.logback.classic.spi.STEUtil;
import com.bxx.pojo.*;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/studentExamAnswer")
public interface StudentExamAnswerClientService {



    /**
     * 多条件获取所有考生考试答案
     * @return
     */
    @GetMapping("/listStudentExamAnswers")
    PageUtil<StudentExamAnswer> listStudentExamAnswers(@RequestBody PageUtil<StudentExamAnswer> pageUtil);
    /**
     * 批改简答题分数
     * @param studentExamAnswer
     * @return
     */
    @PutMapping("/updateStudentExamAnswer")
    R updateStudentExamAnswer(@RequestBody StudentExamAnswer studentExamAnswer);

    @GetMapping("/test")
     List<PageUtil<StudentExamAnswer>> test(@RequestBody PageUtil<StudentExamAnswer> pageUtil);

    /**
     *
     * @param redisVTO
     * @return
     */
    @PostMapping("/addStudentExamAnswerToRedis")
    public R addStudentExamAnswerToRedis(@RequestBody RedisVTO redisVTO);
    /**
     * 自动批改客观题
     * @param pageUtil
     * @return
     */
    @PostMapping("/automaticMarkingOfObjectiveQuestions")
    public R AutomaticMarkingOfObjectiveQuestions(@RequestBody PageUtil<StudentExamAnswer> pageUtil);
}
