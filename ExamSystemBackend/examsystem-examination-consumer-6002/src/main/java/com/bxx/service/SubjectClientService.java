package com.bxx.service;

import com.bxx.pojo.Subject;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/subject")
public interface SubjectClientService {



    /**
     * 多条件获取所有科目
     * @return
     */
    @GetMapping("/listSubjects")
    PageUtil<Subject> listSubjects(@RequestBody PageUtil<Subject> pageUtil);
    /**
     * 添加科目
     * @param subject
     * @return
     */
    @PostMapping("/insertSubject")
    R insertSubject(@RequestBody Subject subject);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    @PutMapping("/updateSubject")
    R updateSubject(@RequestBody Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    @DeleteMapping("/deleteSubject")
    R deleteSubject(@RequestBody String subjectId);

    @RequestMapping(value = "/upload" ,consumes = "multipart/form-data")
   Map<String, Object> upload(@RequestBody MultipartFile file);
}
