package com.bxx.service;

import com.bxx.pojo.Exam;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/exam")
public interface ExamClientService {



    /**
     * 多条件获取所有考试
     * @return
     */
    @GetMapping("/listExams")
    PageUtil<Exam> listExams(@RequestBody PageUtil<Exam> pageUtil);
    /**
     * 添加考试
     * @param exam
     * @return
     */
    @PostMapping("/insertExam")
    R insertExam(@RequestBody Exam exam,@RequestParam("classIds") String classIds);

    /**
     * 修改考试
     * @param exam
     * @return
     */
    @PutMapping("/updateExam")
    R updateExam(@RequestBody Exam exam);

    /**
     * 删除考试
     * @param examId
     * @return
     */
    @DeleteMapping("/cancelExam")
    R cancelExam(@RequestBody String examId);


}
