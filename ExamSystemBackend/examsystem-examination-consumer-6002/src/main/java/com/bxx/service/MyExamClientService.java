package com.bxx.service;

import com.bxx.pojo.Exam;
import com.bxx.pojo.ExamDTO;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-EXAMINATION-PROVIDER")
@RequestMapping("/exam")
public interface MyExamClientService {

    /**
     * 根据用户id获取用户考试列表
     * @param userId
     * @return
     */
    @GetMapping("/getExamsByUserId")
    PageUtil<Exam> getExamsByUserId(@RequestBody String userId);

    /**
     * 开始考试
     * @param examId
     * @return
     */
    @GetMapping("/startExam")
    R startExam(@RequestBody String examId,@RequestParam("userId") String userId);

    //获取试题
    @GetMapping("/getTestQuestions")
    R getTestQuestions(@RequestBody String examId);

    /**
     * 提交考试
     * @param examDTO
     * @return
     */
    @PostMapping("/submitExam")
    R submitExam(@RequestBody ExamDTO examDTO);
}
