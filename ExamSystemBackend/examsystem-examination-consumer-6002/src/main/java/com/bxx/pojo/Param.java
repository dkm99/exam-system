package com.bxx.pojo;

import com.bxx.vo.AutoItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * ClassName: Param
 * Descriptioen:
 * date: 2022/3/31 19:40
 * author :wfl
 */

//专业
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Param implements Serializable {
    TestPaper testPaper;
    AutoItem autoItem;


}
