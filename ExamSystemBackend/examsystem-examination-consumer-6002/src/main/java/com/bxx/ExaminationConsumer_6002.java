package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.bxx.service"})
public class ExaminationConsumer_6002 {

    public static void main(String[] args) {
        SpringApplication.run(ExaminationConsumer_6002.class,args);
    }
}
