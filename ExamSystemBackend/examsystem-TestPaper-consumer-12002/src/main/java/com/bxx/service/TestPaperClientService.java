package com.bxx.service;

import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "EXAMSYSTEM-TESTPAPER-PROVIDER")
@RequestMapping("/testPaper")
public interface TestPaperClientService {



    /**
     * 多条件获取所有试卷
     * @return
     */
    @GetMapping("/listTestPapers")
    PageUtil<TestPaper> listTestPapers(@RequestBody PageUtil<TestPaper> pageUtil);
    /**
     * 添加试卷
     * @param testPaper
     * @return
     */
    @PostMapping("/insertTestPaper")
    R insertTestPaper(@RequestBody TestPaper testPaper,@RequestParam("ids") String ids);

    /**
     * 修改试卷
     * @param testPaper
     * @return
     */
    @PutMapping("/updateTestPaper")
    R updateTestPaper(@RequestBody TestPaper testPaper);

    /**
     * 删除试卷
     * @param testPaperId
     * @return
     */
    @DeleteMapping("/deleteTestPaper")
    R deleteTestPaper(@RequestBody String testPaperId);
    /**
     * 多条件获取所有试卷
     * @return
     */
    @GetMapping("/listTestPaperQuestions")
    PageUtil<Question> listTestPaperQuestions(@RequestBody PageUtil<TestPaper> pageUtil);
}
