package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.bxx.service"})
public class TestPaperConsumer_12002 {

    public static void main(String[] args) {
        SpringApplication.run(TestPaperConsumer_12002.class,args);
    }
}
