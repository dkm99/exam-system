package com.bxx.controller;

import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.service.TestPaperClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer/testPaper")
public class TestPaperController {

    @Autowired
    private TestPaperClientService testPaperClientService;

    @GetMapping("/listTestPapers")
    public PageUtil<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil, TestPaper testPaper) {

        Map<String,Object> map = new HashMap<String, Object>();
        if (testPaper != null){
            map.put("testPaper", testPaper);
        }
        pageUtil.setMap(map);
        return testPaperClientService.listTestPapers(pageUtil);
    }
   //新增试卷
    @PostMapping("/insertTestPaper")
    public R insertTestPaper(TestPaper testPaper,String ids) {
        System.out.println("wwwwwwwwwww="+testPaper.getTestPaperName());
        return testPaperClientService.insertTestPaper(testPaper,ids);
    }
    //修改试卷
    @PutMapping("/updateTestPaper")
    public R updateTestPaper(TestPaper testPaper) {
        System.out.println("consumer.updateTestPaper>"+testPaper);
        return testPaperClientService.updateTestPaper(testPaper);
    }
    //删除试卷
    @DeleteMapping("/deleteTestPaper")
    public R deleteTestPaper(String testPaperId) {
        return testPaperClientService.deleteTestPaper(testPaperId);
    }

    @GetMapping("/listTestPaperQuestions")
    public PageUtil<Question> listTestPaperQuestions(PageUtil<TestPaper> pageUtil, TestPaper testPaper) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (testPaper != null){
            map.put("testPaper", testPaper);
        }
        pageUtil.setMap(map);
        return testPaperClientService.listTestPaperQuestions(pageUtil);
    }
}
