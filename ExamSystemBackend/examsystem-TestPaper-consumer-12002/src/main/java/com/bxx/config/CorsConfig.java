package com.bxx.config;

import com.bxx.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//解决跨域的配置类
//继承WebMvcConfigurerAdapter
@Configuration
public class CorsConfig extends WebMvcConfigurerAdapter {

	@Autowired
    private LoginInterceptor loginInterceptor;
//	@Value("${web.upload-path}")
//	private String uploadFiltPath; // 保存上传文件的路径
//	
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//	    //上传的图片在D盘下的OTA目录下，访问路径如：http://localhost:8081/OTA/d3cf0281-bb7f-40e0-ab77-406db95ccf2c.jpg
//	    //其中OTA表示访问的前缀。"file:D:/OTA/"是文件真实的存储路径
//	    registry.addResourceHandler("/images/**").addResourceLocations("file:" + uploadFiltPath);
//	    super.addResourceHandlers(registry);
//	}
//
	//拦截器
//	@Override
//	public void addInterceptors(InterceptorRegistry registry) {
//		registry.addInterceptor(loginInterceptor)
//        .addPathPatterns("/**").excludePathPatterns("/login/**");
//	}
	
	/**
	 * 注册请求地址
	 * 重写 addCorsMappings
	 */
	public void addCorsMappings(CorsRegistry registry) {
		//设值可以跨域请求的方法allowedMethods()
				//restfull风格里的方法："GET","POST","PUT","DELETE" (注意：大写)
		registry.addMapping("/**").allowedMethods("GET","POST","PUT","DELETE")
		.allowCredentials(true);
	}
}
