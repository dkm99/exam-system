package com.bxx.dao;

import com.bxx.pojo.Exam;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ExamDao {
    /**
     * 多条件获取所有考试
     * @return
     */
    List<Exam> listExams(PageUtil<Exam> pageUtil);

    /**
     * 多条件获取所有考试条数
     * @return
     */
    int listExamsCount(PageUtil<Exam> pageUtil);
    /**
     * 添加考试
     * @param exam
     * @return
     */
    Integer insertExam(Exam exam);

    /**
     * 修改考试
     * @param exam
     * @return
     */
    Integer updateExam(Exam exam);

    /**
     * 删除考试
     * @param examId
     * @return
     */
    Integer deleteExam(String examId);

    /**
     * 通过id查询考试信息
     * @param examId
     * @return
     */
    Exam getExamByExamId(String examId);

    /**
     * 根据考试id集合批量获取考试信息
     * @param examIdList 考试id集合
     * @return
     */
    List<Exam> getExamsByExamIdList(@Param("examIdList") List<String> examIdList);
    /**
     * 根据考试id集合批量获取考试信息数量
     * @param examIdList 考试id集合
     * @return
     */
    Integer getExamsCountByExamIdList(@Param("examIdList") List<String> examIdList);
}
