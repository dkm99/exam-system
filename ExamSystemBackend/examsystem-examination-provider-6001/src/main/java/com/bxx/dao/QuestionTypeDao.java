package com.bxx.dao;

import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface QuestionTypeDao {



    /**
     * 多条件获取所有题目类型
     * @return
     */
    List<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil);
    /**
     * 多条件获取所有题目类型条数
     * @return
     */
    int listQuestionTypesCount(PageUtil<QuestionType> pageUtil);

    /**
     * 查询所有试题题型
     * @return
     */
     List<QuestionType>  getListQuestionTyoes();
    /**
     * 添加题目类型
     * @param questionType
     * @return
     */
    Integer insertQuestionType(QuestionType questionType);

    /**
     * 修改题目类型
     * @param questionType
     * @return
     */
    Integer updateQuestionType(QuestionType questionType);

    /**
     * 删除题目类型
     * @param questionTypeId
     * @return
     */
    Integer deleteQuestionType(String questionTypeId);


}
