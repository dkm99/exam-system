package com.bxx.dao;

import com.bxx.pojo.Subject;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface SubjectDao {
    /**
     * 多条件获取所有科目
     * @return
     */
    List<Subject> listSubjects(PageUtil<Subject> pageUtil);

    /**
     * 多条件获取所有科目条数
     * @return
     */
    int listSubjectCount(PageUtil<Subject> pageUtil);
    /**
     * 添加科目
     * @param subject
     * @return
     */
    Integer insertSubject(Subject subject);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    Integer updateSubject(Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    Integer deleteSubject(String subjectId);


}
