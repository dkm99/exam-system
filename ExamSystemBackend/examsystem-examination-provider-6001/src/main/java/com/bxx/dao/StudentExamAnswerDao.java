package com.bxx.dao;

import com.bxx.pojo.ExamClass;
import com.bxx.pojo.Sequence;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface StudentExamAnswerDao {

    /**
     * 多条件获取所有考试试题答案
     * @return
     */
    List<StudentExamAnswer> listStudentExamAnswers(PageUtil<StudentExamAnswer> pageUtil);
    /**
     * 测试
     * @return
     */
    List<StudentExamAnswer> test(PageUtil<StudentExamAnswer> pageUtil);

    /**
     * 测试count
     * @param pageUtil
     * @return
     */
     int testCount(PageUtil<StudentExamAnswer> pageUtil);
    /**
     * 多条件获取所有考生试题答案条数
     * @return
     */
    int listStudentExamAnswersCount(PageUtil<StudentExamAnswer> pageUtil);
    /**
     * 修改考生试题答案
     * @param studentExamAnswer
     * @return
     */
    Integer updateStudentExamAnswer(StudentExamAnswer studentExamAnswer);

    /**
     * 通过获取考生答案对象
     * @param studentExamAnswerId
     * @return
     */
    StudentExamAnswer getStudentExamAnswerById(String studentExamAnswerId);

    /**
     * 添加学生考试答案
     * @param studentExamAnswer
     * @return
     */
    Integer insertStudentExamAnswer(StudentExamAnswer studentExamAnswer);
}
