package com.bxx.dao;

import com.bxx.pojo.Score;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ScoreDao {

    /**
     * 多条件获取所有考试成绩
     * @return
     */
    List<Score> listScores(PageUtil<Score> pageUtil);
    /**
     * 多条件获取所有考试成绩条数
     * @return
     */
    int listScoresCount(PageUtil<Score> pageUtil);
    /**
     * 修改考试成绩
     * @param score
     * @return
     */
    int updateScore(Score score);

    /**
     * 添加成绩
     * @param score
     * @return
     */
    Integer insertScore(Score score);
}
