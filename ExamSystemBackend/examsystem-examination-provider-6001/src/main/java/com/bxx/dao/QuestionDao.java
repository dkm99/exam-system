package com.bxx.dao;

import com.bxx.pojo.Question;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository //持久层注解
public interface QuestionDao {



    /**
     * 多条件获取所有题目
     * @return
     */
    List<Question> listQuestions(PageUtil<Question> pageUtil);
    /**
     * 多条件获取所有题目的条数
     * @return
     */
    int listQuestionsCount(PageUtil<Question> pageUtil);

    /**
     * 添加题目
     * @param question
     * @return
     */
    Integer insertQuestion(Question question);

    /**
     * 修改题目
     * @param question
     * @return
     */
    Integer updateQuestion(Question question);

    /**
     * 删除题目
     * @param questionId
     * @return
     */
    Integer deleteQuestion(String questionId);
    /**
     * 通过科目查找试题集合
     * @param map
     * @return
     */
    List<Question> getBySubjectIdQuestions(Map map);
    /**
     * 通过科目和试题类型名称获取条数
     * @param map
     * @return
     */
   int getSubjectandNameCount(Map map);

    /**
     * 根据题目id集合获取题目
     * @param questionIdList
     * @return
     */
   List<Question> getQuestionsByQuestionIdList(@Param("questionIdList") List<String> questionIdList);

}
