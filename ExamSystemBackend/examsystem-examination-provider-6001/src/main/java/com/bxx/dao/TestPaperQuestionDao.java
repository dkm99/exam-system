package com.bxx.dao;

import com.bxx.pojo.TestPaperQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface TestPaperQuestionDao {

    /**
     * 根据试卷id获取所有题目id
     * @param testPaperId
     * @return
     */
    List<String> getQuestionIdsByTestPaperId(String testPaperId);

    /**
     * 根据试卷id和题目id获取试卷题目id
     * @param testPaperQuestion
     * @return
     */
    String getTestPaperQuestionIdByTestPaperIdAndQuestionId(TestPaperQuestion testPaperQuestion);
}
