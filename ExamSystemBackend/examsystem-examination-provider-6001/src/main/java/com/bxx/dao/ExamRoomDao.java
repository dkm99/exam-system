package com.bxx.dao;

import com.bxx.pojo.ExamRoom;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ExamRoomDao {
    /**
     * 多条件获取所有考场
     * @return
     */
    List<ExamRoom> listExamRooms(PageUtil<ExamRoom> pageUtil);

    /**
     * 多条件获取所有考场条数
     * @return
     */
    int listExamRoomsCount(PageUtil<ExamRoom> pageUtil);
    /**
     * 添加考场
     * @param examRoom
     * @return
     */
    Integer insertExamRoom(ExamRoom examRoom);

    /**
     * 修改考场
     * @param examRoom
     * @return
     */
    Integer updateExamRoom(ExamRoom examRoom);

    /**
     * 删除考场
     * @param examRoomId
     * @return
     */
    Integer deleteExamRoom(String examRoomId);


}
