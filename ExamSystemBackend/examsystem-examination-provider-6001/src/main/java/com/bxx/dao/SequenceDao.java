package com.bxx.dao;

import com.bxx.pojo.Sequence;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface SequenceDao {
    /**
     * 多条件获取所有批次
     * @return
     */
    List<Sequence> listSequences(PageUtil<Sequence> pageUtil);
    /**
     * 多条件获取所有批次的条数
     * @return
     */
    int listSequencesCount(PageUtil<Sequence> pageUtil);

    /**
     * 添加批次
     * @param sequence
     * @return
     */
    Integer insertSequence(Sequence sequence);

    /**
     * 修改批次
     * @param sequence
     * @return
     */
    Integer updateSequence(Sequence sequence);

    /**
     * 删除批次
     * @param sequenceId
     * @return
     */
    Integer deleteSequence(String sequenceId);

}
