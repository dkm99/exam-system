package com.bxx.dao;

import com.bxx.pojo.StudentExam;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface StudentExamDao {
    int updateStautes(String studentId);

    Integer insertStudentExam(StudentExam studentExam);

    /**
     * 根据学生id和考试id获取学生考试信息
     * @param studentExam
     * @return
     */
    StudentExam getStudentExamByStudentIdAndExamId(StudentExam studentExam);
}
