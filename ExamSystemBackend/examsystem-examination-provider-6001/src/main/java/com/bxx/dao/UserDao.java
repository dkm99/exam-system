package com.bxx.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;



@Mapper
@Repository //持久层注解
public interface UserDao {
    /**
     * 通过班级查询学生数量
     * @param classId
     * @return
     */
    int getUserByClassId(String classId);

    /**
     * 根据用户id获取用户班级id
     * @param userId
     * @return
     */
    String getClassIdByUserId(String userId);


}
