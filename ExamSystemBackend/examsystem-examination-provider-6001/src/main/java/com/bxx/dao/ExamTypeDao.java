package com.bxx.dao;

import com.bxx.pojo.ExamType;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ExamTypeDao {
    /**
     * 多条件获取所有考试类型
     * @return
     */
    List<ExamType> listExamTypes(PageUtil<ExamType> pageUtil);

    /**
     * 多条件获取所有考试类型条数
     * @return
     */
    int listExamTypesCount(PageUtil<ExamType> pageUtil);
    /**
     * 添加考试类型
     * @param examType
     * @return
     */
    Integer insertExamType(ExamType examType);

    /**
     * 修改考试类型
     * @param examType
     * @return
     */
    Integer updateExamType(ExamType examType);

    /**
     * 删除考试类型
     * @param examTypeId
     * @return
     */
    Integer deleteExamType(String examTypeId);


}
