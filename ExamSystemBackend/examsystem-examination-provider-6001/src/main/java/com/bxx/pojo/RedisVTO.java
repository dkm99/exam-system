package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * ClassName: RedisVTO
 * Descriptioen:
 * date: 2022/4/14 19:45
 * author :wfl
 */

@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class RedisVTO {
   String  studentExamAnswerId;
   double   score;
   String textPaperId;
   String studentId;
}
