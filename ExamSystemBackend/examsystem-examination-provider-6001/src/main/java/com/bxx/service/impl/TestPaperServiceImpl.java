package com.bxx.service.impl;

import com.bxx.dao.QuestionDao;
import com.bxx.dao.TestPaperDao;
import com.bxx.pojo.*;
import com.bxx.service.TestPaperService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.AutoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * ClassName: ClassServiceImpl
 * Descriptioen:
 * date: 2022/2/13 20:08
 * author :wfl
 */

@Service
public class TestPaperServiceImpl implements TestPaperService {

    @Autowired
    private TestPaperDao testPaperDao;
    @Autowired
    private QuestionDao questionDao;
    @Override
    public PageUtil<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil) {
        List<TestPaper> testPapers = testPaperDao.listTestPapers(pageUtil);
        int listTestPapersCount = testPaperDao.listTestPapersCount(pageUtil);
        pageUtil.setData(testPapers);
        pageUtil.setCount(listTestPapersCount);
        System.out.println("试卷的长度"+listTestPapersCount);
        return pageUtil;

    }



    @Override
    public R insertTestPaper(TestPaper testPaper, String ids, AutoItem autoItem) {

        System.out.println("11"+ids.equals("null"));
        System.out.println("111"+!ids.equals("null"));


        //通过uuid生成考试id
        String testPaperId = UUID.randomUUID().toString();
        testPaper.setTestPaperId(testPaperId);
        Integer r = testPaperDao.insertTestPaper(testPaper);

        if(ids.equals("null")){ //自动组卷
            System.out.println("我终于执行了");
            System.out.println("sdsasd"+autoItem.getSubjectId());
            List<List<Question>> qs=new ArrayList<>();
            //根据科目得到本科目下所有的试题；
            List<Question> choosequestions=getBySubjectIdQuestions(autoItem.getSubjectId(),"单选",autoItem.getChoose());
            List<Question> completionquestions=getBySubjectIdQuestions(autoItem.getSubjectId(),"填空",autoItem.getCompletion());
            List<Question> judgmentquestions=getBySubjectIdQuestions(autoItem.getSubjectId(),"判断题",autoItem.getJudgment());
            List<Question> schoolProfilequestions=getBySubjectIdQuestions(autoItem.getSubjectId(),"主观题",autoItem.getSchoolProfile());
            qs.add(choosequestions);
            qs.add(completionquestions);
            qs.add(judgmentquestions);
            qs.add(schoolProfilequestions);
            System.out.println("sss="+qs.size());
            System.out.println("ge==="+qs.get(0).get(0));
            for(int i=0;i<qs.size();i++){
                System.out.println("我是"+qs.get(i).size());
                for(int j=0; j<qs.get(i).size();j++){
                    if(qs.get(i).size()>0){//判断不同类型的集合长度如果大于0添加到中间表中
                        //创建中间表对象
                        TestPaperQuestion testPaperQuestion = new TestPaperQuestion();
                        testPaperQuestion.setTestPaperQuestionId(UUID.randomUUID().toString());
                        testPaperQuestion.setTestPaperId(testPaperId);
                        testPaperQuestion.setQuestionId(qs.get(i).get(j).getQuestionId());
                        testPaperDao.insertTestPaperQuestion(testPaperQuestion);
                    }

                }
            }


            return  R.ok("新增成功");

        }else{//手动组卷
            if(r>0){
                System.out.println("questionId==>>"+ids);
                String[] mid=ids.split(",");
                System.out.println("99"+mid.length);
                System.out.println("100+"+mid[0]);
                if(ids != "1" || !ids.equals("1") ) {
                    for(int i =0;i<mid.length;i++) {
                        //同过遍历取出字符串里的每个id
                        String   id=mid[i];
                        TestPaperQuestion testPaperQuestion = new TestPaperQuestion();
                        testPaperQuestion.setTestPaperQuestionId(UUID.randomUUID().toString());
                        testPaperQuestion.setTestPaperId(testPaperId);
                        testPaperQuestion.setQuestionId(id);
                        testPaperDao.insertTestPaperQuestion(testPaperQuestion);
                    }
                }

                return R.ok("新增成功");
            }else {
                return R.error("新增失败");
            }

        }


    }

    @Transactional //开启事务
    @Override
    public R updateTestPaper(TestPaper testPaper) {
        Integer r = testPaperDao.updateTestPaper(testPaper);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteTestPaper(String testPaperId) {
        System.out.println("testPaperId===="+testPaperId);
        Integer r = testPaperDao.deleteTestPaper(testPaperId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }

    @Override
    public PageUtil<Question> listTestPaperQuestions(PageUtil<TestPaper> pageUtil) {
        List<TestPaper> testPapers = testPaperDao.listTestPaperQuestions(pageUtil);
        PageUtil<Question> pageUtil1=new PageUtil<Question>();
        List<Question> Questions=new ArrayList<>();
        if(testPapers!=null&testPapers.size()>0){
            for(int i = 0; i < testPapers.size(); i++){
                if(testPapers.get(0).getQuestions()!=null&testPapers.get(0).getQuestions().size()>0){
                    for(int j=0;j<testPapers.get(0).getQuestions().size();j++){
                        Questions = testPapers.get(0).getQuestions();

                    }
                }
            }
        }
        pageUtil1.setData(Questions);
        return  pageUtil1;
    }

    @Override
    public R autoTestPaper(String subjectId, String testPaperId) {
//        //该科目下所有试题
//        List<Question> listQuestion = questionDao.getBySubjectIdQuestions(subjectId);
//        //乱序
//        Collections.shuffle(listQuestion);
//        List<Question> L1=new ArrayList<Question>() ;
//        List<Question> L2=new ArrayList<Question>() ;
//        List<Question> L3=new ArrayList<Question>() ;
//        for(int i=0;i<listQuestion.size();i++){
//            if(listQuestion.get(i).getQuestionTypeId()=="4fb62c55-7b07-4092-bb22-4f7c8669688a"){
//                L1.add(listQuestion.get(i));
//            }else{
//                L2.add(listQuestion.get(i));
//            }
//        }



        return null;
    }

    /**
     * 根据试题类型和科目类型获得所需要的试题数量
     * @param subjectId
     * @param questionTypeName
     * @param num
     * @return
     */
    @Override
    public List<Question> getBySubjectIdQuestions(String subjectId, String questionTypeName, int num) {
        Map hashMap = new HashMap();
        System.out.println("questionTypeName"+questionTypeName);
        hashMap.put("subjectId",subjectId);
        hashMap.put("questionTypeName",questionTypeName);

        List<Question> listQuestion = questionDao.getBySubjectIdQuestions(hashMap);
        //乱序
        Collections.shuffle(listQuestion);
        //取出前台填写的条数
        System.out.println("listQuestion.size()++"+listQuestion.size());
        System.out.println("num"+num);
        listQuestion = listQuestion.subList(0, num);
        return listQuestion;
    }


}
