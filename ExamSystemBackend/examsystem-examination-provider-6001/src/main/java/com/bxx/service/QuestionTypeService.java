package com.bxx.service;


import com.bxx.pojo.QuestionType;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface QuestionTypeService {

	/**
	 * 多条件获取所有题目类型
	 * @return
	 */
	PageUtil<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil);

	/**
	 * 添加题目类型
	 * @param questionType
	 * @return
	 */
	R insertQuestionType(QuestionType questionType);

	/**
	 * 修改题目类型
	 * @param questionType
	 * @return
	 */
	R  updateQuestionType(QuestionType questionType);

	/**
	 * 删除题目类型
	 * @param questionTypeId
	 * @return
	 */
	R  deleteQuestion(String questionTypeId);
}
