package com.bxx.service;

import com.bxx.pojo.ExamRoom;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface ExamRoomService {

    /**
     * 多条件获取所有考场
     * @return
     */
    PageUtil<ExamRoom> listExamRooms(PageUtil<ExamRoom> pageUtil);


    /**
     * 添加考场
     * @param examRoom
     * @return
     */
    R insertExamRoom(ExamRoom examRoom);

    /**
     * 修改考场
     * @param examRoom
     * @return
     */
    R updateExamRoom(ExamRoom examRoom);

    /**
     * 删除考场
     * @param examRoomId
     * @return
     */
    R deleteExamRoom(String examRoomId);



}
