package com.bxx.service.impl;

import com.bxx.dao.SequenceDao;
import com.bxx.pojo.Sequence;
import com.bxx.service.SequenceService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * ClassName: SubjectServiceImpl
 * Descriptioen:
 * date: 2022/3/26 21:47
 * author :wfl
 */

@Service
public class SequenceServiceImpl implements SequenceService {
    //用户持久层对象
    @Autowired
    private SequenceDao sequenceDao;



    @Override
    public PageUtil<Sequence> listSequences(PageUtil<Sequence> pageUtil) {
        List<Sequence> sequences = sequenceDao.listSequences(pageUtil);
        int count = sequenceDao.listSequencesCount(pageUtil);
        pageUtil.setData(sequences);
        pageUtil.setCount(count);
        return pageUtil;
    }


    @Override
    public R insertSequence(Sequence sequence) {
        sequence.setSequenceId(UUID.randomUUID().toString());
         Integer r = sequenceDao.insertSequence(sequence);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateSequence(Sequence sequence) {
        Integer r = sequenceDao.updateSequence(sequence);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteSequence(String sequenceId) {
        System.out.println("sequenceId===="+sequenceId);
        Integer r = sequenceDao.deleteSequence(sequenceId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }


}
