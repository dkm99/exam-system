package com.bxx.service;

import com.bxx.pojo.Sequence;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface SequenceService {

    /**
     * 多条件获取所有批次
     * @return
     */
    PageUtil<Sequence> listSequences(PageUtil<Sequence> pageUtil);


    /**
     * 添加批次
     * @param sequence
     * @return
     */
    R insertSequence(Sequence sequence);

    /**
     * 修改批次
     * @param sequence
     * @return
     */
    R updateSequence(Sequence sequence);

    /**
     * 删除批次
     * @param sequenceId
     * @return
     */
    R deleteSequence(String sequenceId);


}
