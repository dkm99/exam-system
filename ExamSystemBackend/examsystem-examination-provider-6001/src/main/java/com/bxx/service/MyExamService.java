package com.bxx.service;

import com.bxx.pojo.Exam;
import com.bxx.pojo.Question;
import com.bxx.pojo.ExamDTO;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TestPaperQuestionVO;

import java.util.List;

public interface MyExamService {

    /**
     * 根据用户id获取用户考试列表
     * @param userId
     * @return
     */
    PageUtil<Exam> getExamsByUserId(String userId);

    /**
     * 开始考试
     * @param examId
     * @return
     */
    R startExam(String examId, String userId);

    /**
     * 根据考试获取试题
     * @param examId
     * @return
     */
    R getTestQuestions(String examId);

    /**
     * 格式题目集合按照类型分类
     * @param questions
     * @return
     */
    List<TestPaperQuestionVO> formatQuestions(List<Question> questions);

    /**
     * 提交考试
     * @param examDTO
     * @return
     */
    R submitExam(ExamDTO examDTO);
}
