package com.bxx.service;

import com.bxx.pojo.RedisVTO;
import com.bxx.pojo.Sequence;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

import java.util.List;

public interface StudentExamAnswerService {
    /**
     * 多条件获取所有考试试题答案
     * @return
     */
    PageUtil<StudentExamAnswer> listStudentExamAnswers(PageUtil<StudentExamAnswer> pageUtil);
    /**
     * 修改考生试题答案
     * @param studentExamAnswer
     * @return
     */
    R updateStudentExamAnswer(StudentExamAnswer studentExamAnswer);

    List<PageUtil<StudentExamAnswer>> test(PageUtil<StudentExamAnswer> pageUtil);

    /**
     * 将页面批改的信息添加到redis
     *
     * @return
     */
    R addStudentExamAnswerToRedis(RedisVTO redisVTO);

    /**
     * 自动批阅客观题
     * @return
     */
   R AutomaticMarkingOfObjectiveQuestions(PageUtil<StudentExamAnswer> pageUtil);
}
