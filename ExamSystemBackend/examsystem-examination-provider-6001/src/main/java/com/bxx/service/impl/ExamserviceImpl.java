package com.bxx.service.impl;

import com.bxx.dao.ExamClassDao;
import com.bxx.dao.ExamDao;
import com.bxx.dao.UserDao;
import com.bxx.pojo.Exam;
import com.bxx.pojo.ExamClass;
import com.bxx.pojo.TestPaperQuestion;
import com.bxx.service.ExamService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: SubjectServiceImpl
 * Descriptioen:
 * date: 2022/3/26 21:47
 * author :wfl
 */

@Service
public class ExamserviceImpl implements ExamService {
    //考试持久层对象
    @Autowired
    private ExamDao examDao;
    //考试——班级持久层对象
    @Autowired
    private ExamClassDao examClassDao;
    //用户持久层对象
    @Autowired
    private UserDao  userDao;


    @Override
    public PageUtil<Exam> listExams(PageUtil<Exam> pageUtil) {
        List<Exam> exams = examDao.listExams(pageUtil);
        int count = examDao.listExamsCount(pageUtil);
        pageUtil.setData(exams);
        pageUtil.setCount(count);
        return pageUtil;
    }

    /**
     * 发布考试
     * @param exam
     * @param classIds 在发布考试中将班级信息和考试相关联
     * @return
     */
    @Override
    public R insertExam(Exam exam,String classIds) {
        System.out.println("classIdS======"+classIds);
        //生成考试Id
        String examId = UUID.randomUUID().toString();
        String[] cid=classIds.split(",");
        System.out.println("99"+cid.length);
        System.out.println("100+"+cid[0]);
        if(cid != null ) {
            int count=0;
            for(int i =0;i<cid.length;i++) {
                //通过遍历取出字符串里的每个id
                String   classId=cid[i];
                //得到该班级下的学生人数
                System.out.println("classid=++++"+classId);
                int userByClassIdCount = userDao.getUserByClassId(classId);
                count+=userByClassIdCount;
                //将考试-班级关联信息添加到中间表中
                ExamClass examClass = new ExamClass();
                examClass.setClassId(classId);
                examClass.setExamId(examId);
                examClass.setCreateTime(DateParse.parseTimeStamp(new Date()));
                examClassDao.insertExamClass(examClass);
            }
            //将人数添加到考试信息中
            exam.setExamPeopleNum(count);
            exam.setExamStatus(0);
            exam.setExamId(examId);
            Integer r = examDao.insertExam(exam);
            if(r>0){
                return R.ok("新增成功");
            }else{
                return R.error("新增失败");
            }
        }else{
            return R.error("新增失败");
        }
       }

    @Transactional //开启事务
    @Override
    public R updateExam(Exam exam) {
        Integer r = examDao.updateExam(exam);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    /**
     * 修改状态
     * @param examId
     * @return
     */
    @Transactional //开启事务
    @Override
    public R cancelExam(String examId) {
        System.out.println("examId===="+examId);
        Exam examByExamId = examDao.getExamByExamId(examId);
        //设置为已取消，默认状态3为考试已取消
        examByExamId.setExamStatus(3);
        int r = examDao.updateExam(examByExamId);
        if (r > 0) {
            return R.ok("取消成功");
        } else {
            return R.error("取消失败");
        }
    }




}
