package com.bxx.service;

import com.bxx.pojo.Subject;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface SubjectService {

    /**
     * 多条件获取所有科目
     * @return
     */
    PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil);


    /**
     * 添加科目
     * @param subject
     * @return
     */
    R insertSubject(Subject subject);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    R updateSubject(Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    R deleteSubject(String subjectId);

    /**
     * 上传图片
     * @param file
     * @param
     * @return
     */
    Map<String, Object> upload(MultipartFile file);

}
