package com.bxx.service.impl;

import com.bxx.dao.ExamRoomDao;
import com.bxx.pojo.ExamRoom;
import com.bxx.service.ExamRoomService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * ClassName: SubjectServiceImpl
 * Descriptioen:
 * date: 2022/3/26 21:47
 * author :wfl
 */

@Service
public class ExamRoomerviceImpl implements ExamRoomService {
    //考场持久层对象
    @Autowired
    private ExamRoomDao examRoomDao;



    @Override
    public PageUtil<ExamRoom> listExamRooms(PageUtil<ExamRoom> pageUtil) {
        List<ExamRoom> examRooms = examRoomDao.listExamRooms(pageUtil);
        int count = examRoomDao.listExamRoomsCount(pageUtil);
        pageUtil.setData(examRooms);
        pageUtil.setCount(count);
        return pageUtil;
    }


    @Override
    public R insertExamRoom(ExamRoom examRoom) {
        examRoom.setExamRoomId(UUID.randomUUID().toString());
         Integer r = examRoomDao.insertExamRoom(examRoom);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateExamRoom(ExamRoom examRoom) {
        Integer r = examRoomDao.updateExamRoom(examRoom);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteExamRoom(String examRoomId) {
        System.out.println("examRoomId===="+examRoomId);
        Integer r = examRoomDao.deleteExamRoom(examRoomId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }




}
