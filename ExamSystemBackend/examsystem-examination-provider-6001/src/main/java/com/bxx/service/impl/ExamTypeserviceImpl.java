package com.bxx.service.impl;

import com.bxx.dao.ExamTypeDao;
import com.bxx.pojo.ExamType;
import com.bxx.service.ExamTypeService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * ClassName: SubjectServiceImpl
 * Descriptioen:
 * date: 2022/3/26 21:47
 * author :wfl
 */

@Service
public class ExamTypeserviceImpl implements ExamTypeService {
    //考试类型持久层对象
    @Autowired
    private ExamTypeDao examTypeDao;



    @Override
    public PageUtil<ExamType> listExamTypes(PageUtil<ExamType> pageUtil) {
        List<ExamType> examTypes = examTypeDao.listExamTypes(pageUtil);
        int count = examTypeDao.listExamTypesCount(pageUtil);
        pageUtil.setData(examTypes);
        pageUtil.setCount(count);
        return pageUtil;
    }


    @Override
    public R insertExamType(ExamType examType) {
        examType.setExamTypeId(UUID.randomUUID().toString());
         Integer r = examTypeDao.insertExamType(examType);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateExamType(ExamType examType) {
        Integer r = examTypeDao.updateExamType(examType);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteExamType(String examTypeId) {
        System.out.println("examTypeId===="+examTypeId);
        Integer r = examTypeDao.deleteExamType(examTypeId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }




}
