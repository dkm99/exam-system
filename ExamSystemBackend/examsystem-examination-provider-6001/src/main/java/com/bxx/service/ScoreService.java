package com.bxx.service;

import com.bxx.pojo.Score;
import com.bxx.pojo.Sequence;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface ScoreService {
    /**
     * 多条件获取所有考试成绩
     * @return
     */
    PageUtil<Score> listScores(PageUtil<Score> pageUtil);
    /**
     * 修改考试成绩
     * @param score
     * @return
     */
    R Rewinding(Score score);



}
