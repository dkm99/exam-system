package com.bxx.service;


import com.bxx.pojo.Question;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface QuestionService {

	/**
	 * 多条件获取所有题目
	 * @return
	 */
	PageUtil<Question> listQuestions(PageUtil<Question> pageUtil);
	/**
	 * 多条件获取所有题目的条数
	 * @return
	 */

	R insertQuestion(Question question);

	/**
	 * 修改题目
	 * @param question
	 * @return
	 */
	R updateQuestion(Question question);

	/**
	 * 删除题目
	 * @param questionId
	 * @return
	 */
	R deleteQuestion(String questionId);
	/**
	 * 通过科目和试题类型名称获取条数
	 * @param
	 * @return
	 */
	int getSubjectandNameCount(String subjectId,String questionTypeName);
}
