package com.bxx.service.impl;

import com.bxx.dao.QuestionDao;
import com.bxx.pojo.Question;
import com.bxx.service.QuestionService;
import com.bxx.util.DateParse;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class QuesionServiceImpl implements QuestionService {

	//用户持久层对象
	@Autowired
	private QuestionDao questionDao;
	


	@Override
	public PageUtil<Question> listQuestions(PageUtil<Question> pageUtil) {
		List<Question> questions = questionDao.listQuestions(pageUtil);
		System.out.println("sagd"+questions.get(0).getSubjectId());
		int count = questionDao.listQuestionsCount(pageUtil);
		pageUtil.setData(questions);
		pageUtil.setCount(count);
		return pageUtil;
	}


	@Override
	public R insertQuestion(Question question) {
		question.setQuestionId(UUID.randomUUID().toString());
		question.setQuestionCreateTime(DateParse.parseTimeStamp(new Date()));
		Integer r = questionDao.insertQuestion(question);
		if(r>0){
			return R.ok("新增成功");
		}else {
			return R.error("新增失败");
		}
	}

	@Transactional //开启事务
	@Override
	public R updateQuestion(Question question) {
		Integer r = questionDao.updateQuestion(question);

		if(r>0){
			return R.ok("修改成功");
		}else {
			return R.error("修改失败");
		}
	}

	@Transactional //开启事务
	@Override
	public R deleteQuestion(String questionId) {
		System.out.println("classid===="+questionId);
		Integer r = questionDao.deleteQuestion(questionId);
		if (r > 0) {
			return R.ok("删除成功");
		} else {
			return R.error("删除失败");
		}
	}

	@Override
	public int getSubjectandNameCount(String subjectId, String questionTypeName) {
		Map hashMap = new HashMap();
		hashMap.put("subjectId",subjectId);
		if(questionTypeName.equals("choose")){
			hashMap.put("questionTypeName","单选");

		}else if(questionTypeName.equals("completion")){
			hashMap.put("questionTypeName","填空");
		}else if(questionTypeName.equals("judgment")){
			hashMap.put("questionTypeName","判断");
		}else{
			hashMap.put("questionTypeName","简答");
		}
          

		return questionDao.getSubjectandNameCount(hashMap);
	}


}
