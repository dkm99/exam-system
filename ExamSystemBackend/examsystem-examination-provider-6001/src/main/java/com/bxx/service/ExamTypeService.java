package com.bxx.service;

import com.bxx.pojo.ExamType;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface ExamTypeService {

    /**
     * 多条件获取所有考试类型
     * @return
     */
    PageUtil<ExamType> listExamTypes(PageUtil<ExamType> pageUtil);


    /**
     * 添加考试类型
     * @param examType
     * @return
     */
    R insertExamType(ExamType examType);

    /**
     * 修改考试类型
     * @param examType
     * @return
     */
    R updateExamType(ExamType examType);

    /**
     * 删除考试类型
     * @param examTypeId
     * @return
     */
    R deleteExamType(String examTypeId);



}
