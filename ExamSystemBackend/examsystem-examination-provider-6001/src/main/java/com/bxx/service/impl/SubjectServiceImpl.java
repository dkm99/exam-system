package com.bxx.service.impl;

import com.bxx.dao.SubjectDao;
import com.bxx.pojo.Subject;
import com.bxx.service.SubjectService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * ClassName: SubjectServiceImpl
 * Descriptioen:
 * date: 2022/3/26 21:47
 * author :wfl
 */

@Service
public class SubjectServiceImpl implements SubjectService {
    //用户持久层对象
    @Autowired
    private SubjectDao subjectDao;



    @Override
    public PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil) {
        List<Subject> questions = subjectDao.listSubjects(pageUtil);
        int count = subjectDao.listSubjectCount(pageUtil);
        pageUtil.setData(questions);
        pageUtil.setCount(count);
        return pageUtil;
    }


    @Override
    public R insertSubject(Subject subject) {
        subject.setSubjectId(UUID.randomUUID().toString());
         Integer r = subjectDao.insertSubject(subject);
        if(r>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateSubject(Subject subject) {
        Integer r = subjectDao.updateSubject(subject);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteSubject(String subjectId) {
        System.out.println("classid===="+subjectId);
        Integer r = subjectDao.deleteSubject(subjectId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }


    @Override
    public Map<String, Object> upload(MultipartFile file) {
        String prefix = "";
        // 保存上传
        OutputStream out = null;
        InputStream fileInput = null;

        System.out.println("file" + file);
        try {
            if (file != null) {
                String originalName = file.getOriginalFilename();
                prefix = originalName.substring(originalName.lastIndexOf(".") + 1);
                String uuid = UUID.randomUUID() + "";

                // String filepath = "../static/front/img/"+uuid+"." + prefix;

                 //D:\ExamDemo\exam-system\ExamSystemFrontend
                //String filepath = s + "/static/front/img/" + uuid + "." + prefix;

                String filepath="D:\\ExamDemo\\exam-system\\ExamSystemFrontend\\admin\\img\\" + uuid + "." + prefix;

                File files = new File(filepath);
                // 打印查看上传路径
                System.out.println(filepath);
                if (!files.getParentFile().exists()) {
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                Map<String, Object> map2 = new HashMap<>();
                Map<String, Object> map = new HashMap<>();
                map.put("code", 0);
                map.put("msg", "");
                map.put("data", map2);
                map2.put("src", uuid + "." + prefix);
                return map;
            }

        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "");
        return map;

    }


}
