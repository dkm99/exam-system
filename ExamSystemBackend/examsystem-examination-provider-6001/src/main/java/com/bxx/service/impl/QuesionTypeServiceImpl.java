package com.bxx.service.impl;

import com.bxx.dao.QuestionTypeDao;
import com.bxx.pojo.QuestionType;
import com.bxx.service.QuestionTypeService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class QuesionTypeServiceImpl implements QuestionTypeService {

	//用户持久层对象
	@Autowired
	private QuestionTypeDao questionTypeDao;


	@Override
	public PageUtil<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil) {

		List<QuestionType> questionTypes = questionTypeDao.listQuestionTypes(pageUtil);

		int count = questionTypeDao.listQuestionTypesCount(pageUtil);
		pageUtil.setData(questionTypes);
		pageUtil.setCount(count);
		return pageUtil;
	}

	@Override
	public R insertQuestionType(QuestionType questionType) {
		questionType.setQuestionTypeId(UUID.randomUUID().toString());
		Integer r = questionTypeDao.insertQuestionType(questionType);
		if(r>0){
			return R.ok("新增成功");
		}else {
			return R.error("新增失败");
		}
	}

	@Override
	public R updateQuestionType(QuestionType questionType) {
		Integer r = questionTypeDao.updateQuestionType(questionType);

		if(r>0){
			return R.ok("修改成功");
		}else {
			return R.error("修改失败");
		}
	}

	@Override
	public R deleteQuestion(String questionTypeId) {
		System.out.println("questionTypeId===="+questionTypeId);
		Integer r = questionTypeDao.deleteQuestionType(questionTypeId);
		if (r > 0) {
			return R.ok("删除成功");
		} else {
			return R.error("删除失败");
		}
	}
}
