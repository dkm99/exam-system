package com.bxx.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.bxx.dao.ExamDao;
import com.bxx.dao.QuestionTypeDao;
import com.bxx.dao.StudentExamAnswerDao;
import com.bxx.pojo.ExamRoom;
import com.bxx.pojo.QuestionType;
import com.bxx.pojo.RedisVTO;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.service.StudentExamAnswerService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StudentExamAnswerServiceImpl implements StudentExamAnswerService {

    //久层对象
    @Autowired
    private StudentExamAnswerDao StudentExamAnswerDao;
    //试题类型持久层对象
    @Autowired
    private QuestionTypeDao questionTypeDao;
    //redis缓存对象
    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public PageUtil<StudentExamAnswer> listStudentExamAnswers(PageUtil<StudentExamAnswer> pageUtil) {
        List<StudentExamAnswer> answerList = StudentExamAnswerDao.listStudentExamAnswers(pageUtil);
        int count = StudentExamAnswerDao.listStudentExamAnswersCount(pageUtil);
        for(int i=0;i<answerList.size();i++){
            //判断是否为简答题，若不是简答题则和答案进行比较
            if(!answerList.get(i).getTestPaperQuestion().getQuestion().getQuestionType().getQuestionTypeName().equals("简答")){
                //考生答案
                String examStudentanswer = answerList.get(i).getAnswer();
                //试题答案
                String questionAnswer = answerList.get(i).getTestPaperQuestion().getQuestion().getQuestionAnswer();
                //如果值相等则证明试题正确
                if(examStudentanswer.equals(questionAnswer)){
                    //试题分值
                    Double questionScore = answerList.get(i).getTestPaperQuestion().getQuestion().getQuestionScore();
                    answerList.get(i).setScore(questionScore);
                }else{//试题错误
                    answerList.get(i).setScore(0.0);

                }
                //自动改题

            }else{


            }
            StudentExamAnswerDao.updateStudentExamAnswer(answerList.get(i));
        }

        pageUtil.setData(answerList);
        System.out.println("count==="+count);
        pageUtil.setCount(count);
        return pageUtil;

    }
    public List<PageUtil<StudentExamAnswer>>  test(PageUtil<StudentExamAnswer> pageUtil){
        //查询出所有的题型
        List<QuestionType> listQuestionTyoes = questionTypeDao.getListQuestionTyoes();
        List<PageUtil<StudentExamAnswer>> list=new ArrayList<>();
        String questionTypeName=null;
        //用来存放每次生成的集合
        PageUtil<StudentExamAnswer> test=null;

        //每循环一次添加一个分页类放入list
       for (int i=0;i<listQuestionTyoes.size();i++){
           //创建分页类对象
           test=new PageUtil<>();
           Map<String,Object> map = new HashMap<String, Object>();
           Object studentId = pageUtil.getMap().get("studentId");

           if (listQuestionTyoes.get(i).getQuestionTypeName() != null){
               map.put("questionTypeName", listQuestionTyoes.get(i).getQuestionTypeName());
               map.put("studentId", studentId);
           }
           test.setMap(map);
           System.out.println("map的内容"+test.getMap());
          // pageUtil.setMap(map);
           //查询出每个分页类集合下的数据
           List<StudentExamAnswer> test1 = StudentExamAnswerDao.test(test);
           for (int j=0;j<test1.size();j++){
               if(test1.get(j).getAnswer().equals(test1.get(j).getTestPaperQuestion().getQuestion().getQuestionAnswer())){
                   test1.get(j).setScore(test1.get(j).getTestPaperQuestion().getQuestion().getQuestionScore());
               }else{
                   test1.get(j).setScore(0.0);
               }
               }
           int count = StudentExamAnswerDao.testCount(test);
           //将数据存放到分页工具类中   PageUtil<StudentExamAnswer>
           test.setData(test1);
           test.setCount(count);
            list.add(test);


        }
        return  list;
    }

    @Override
    public R addStudentExamAnswerToRedis(RedisVTO redisVTO) {
        System.out.println("sssss:"+redisVTO.getScore());

        //获取对象
        StudentExamAnswer studentExamAnswerById = StudentExamAnswerDao.getStudentExamAnswerById(redisVTO.getStudentExamAnswerId());
        studentExamAnswerById.setScore(redisVTO.getScore());
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(redisVTO.getStudentId()+redisVTO.getTextPaperId());
        String s = JSON.toJSONString(studentExamAnswerById);
        hashOps.put(redisVTO.getStudentExamAnswerId(),s);
            return  R.ok("批改成功");

    }

    @Override
    public R AutomaticMarkingOfObjectiveQuestions(PageUtil<StudentExamAnswer> pageUtil) {
        //将该学生和试卷下的数据进行存到redis上
        List<StudentExamAnswer> test = StudentExamAnswerDao.test(pageUtil);
        if (test!=null){
            for(int i=0;i<test.size();i++){
                //如果不为简答题则进行保存到redis
               if(!test.get(i).getTestPaperQuestion().getQuestion().getQuestionType().getQuestionTypeName().equals("简答")){
                    String studentExamAnswerId=test.get(i).getStudentExamAnswerId();
                    StudentExamAnswer studentExamAnswerById = StudentExamAnswerDao.getStudentExamAnswerById(studentExamAnswerId);
                    //判断考生答案是否正确，保存分数
                    System.out.println(studentExamAnswerById.getAnswer().equals(test.get(i).getTestPaperQuestion().getQuestion().getQuestionAnswer()));
                    if(studentExamAnswerById.getAnswer().equals(test.get(i).getTestPaperQuestion().getQuestion().getQuestionAnswer())){
                        studentExamAnswerById.setScore(test.get(i).getTestPaperQuestion().getQuestion().getQuestionScore());
                    }else{
                        studentExamAnswerById.setScore(0.0);
                    }

                    BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(test.get(i).getStudentId()+pageUtil.getMap().get("textPaperId"));
                    String s = JSON.toJSONString(studentExamAnswerById);
                    hashOps.put(test.get(i).getStudentExamAnswerId(),s);

                }else{

                }
            }
            return  R.ok("批改成功");
        }else {
            return  R.error("批改失败");
        }

    }

    @Override
    public R updateStudentExamAnswer(StudentExamAnswer studentExamAnswer) {
        Integer r = StudentExamAnswerDao.updateStudentExamAnswer(studentExamAnswer);
        if(r>0){
            return R.ok("批改成功");
        }else {
            return R.error("批改失败");
        }
    }

}
