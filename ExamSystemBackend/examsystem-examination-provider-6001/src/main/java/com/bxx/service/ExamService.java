package com.bxx.service;

import com.bxx.pojo.Exam;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

public interface ExamService {

    /**
     * 多条件获取所有考试
     * @return
     */
    PageUtil<Exam> listExams(PageUtil<Exam> pageUtil);


    /**
     * 添加考试
     * @param exam
     * @return
     */
    R insertExam(Exam exam,String classIds);

    /**
     * 修改考试
     * @param exam
     * @return
     */
    R updateExam(Exam exam);

    /**
     * 取消考试
     * @param examId
     * @return
     */
    R cancelExam(String examId);



}
