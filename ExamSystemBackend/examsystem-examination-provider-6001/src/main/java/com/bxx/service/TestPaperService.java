package com.bxx.service;

import com.bxx.vo.AutoItem;
import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

import java.util.List;

public interface TestPaperService {
    /**
     * 多条件获取所有试卷
     * @return
     */
    PageUtil<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil);


    /**
     * 添加试卷
     * @param testPaper
     * @return
     */
    R insertTestPaper(TestPaper testPaper, String ids, AutoItem autoItem);

    /**
     * 修改试卷
     * @param testPaper
     * @return
     */
    R updateTestPaper(TestPaper testPaper);

    /**
     * 删除试卷
     * @param testPaperId
     * @return
     */
    R deleteTestPaper(String testPaperId);
    PageUtil<Question> listTestPaperQuestions(PageUtil<TestPaper> pageUtil);
    /**
     * 自动组卷
     * @param subjectId
     * @param testPaperId
     * @return
     */
    R  autoTestPaper(String subjectId,String testPaperId);

    /**
     * 根据试题类型和科目类型获得所需要的试题数量
     * @param subjectId
     * @param questionTypeName
     * @param num
     * @return
     */
    List<Question> getBySubjectIdQuestions(String subjectId,String questionTypeName,int num);

}
