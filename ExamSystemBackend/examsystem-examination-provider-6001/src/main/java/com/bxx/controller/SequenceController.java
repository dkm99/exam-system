package com.bxx.controller;

import com.bxx.pojo.Sequence;
import com.bxx.service.SequenceService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/sequence")
public class SequenceController {
    @Autowired
    private SequenceService sequenceService;


    @GetMapping("/listSequences")
    public PageUtil<Sequence> listSequences(@RequestBody PageUtil<Sequence> pageUtil) {

        return sequenceService.listSequences(pageUtil);
    }
    //新增批次
    @PostMapping("/insertSequence")
    public R insertSequence(@RequestBody Sequence sequence) {
        return sequenceService.insertSequence(sequence);
    }
    //修改批次
    @PutMapping("/updateSequence")
    public R updateSequence(@RequestBody Sequence sequence) {
        return sequenceService.updateSequence(sequence);
    }
    //删除批次
    @DeleteMapping("/deleteSequence")
    public R deleteSequence(@RequestBody String sequenceId) {
        System.out.println("sequenceId=="+sequenceId);
        return sequenceService.deleteSequence(sequenceId);
    }




}
