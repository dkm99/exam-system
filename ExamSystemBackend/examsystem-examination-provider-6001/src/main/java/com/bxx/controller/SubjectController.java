package com.bxx.controller;

import com.bxx.pojo.Subject;
import com.bxx.service.SubjectService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;


    @GetMapping("/listSubjects")
    public PageUtil<Subject> listSubjects(@RequestBody PageUtil<Subject> pageUtil) {

        return subjectService.listSubjects(pageUtil);
    }
    //新增科目
    @PostMapping("/insertSubject")
    public R insertSubject(@RequestBody Subject subject) {
        return subjectService.insertSubject(subject);
    }
    //修改科目
    @PutMapping("/updateSubject")
    public R updateSubject(@RequestBody Subject subject) {
        return subjectService.updateSubject(subject);
    }
    //删除科目
    @DeleteMapping("/deleteSubject")
    public R deleteSubject(@RequestBody String subjectId) {
        System.out.println("questionId=="+subjectId);
        return subjectService.deleteSubject(subjectId);
    }

    // 歌曲上传
    @RequestMapping("upload")
    public Map<String, Object> upload(@RequestBody MultipartFile file) {

        return subjectService.upload(file);

    }


}
