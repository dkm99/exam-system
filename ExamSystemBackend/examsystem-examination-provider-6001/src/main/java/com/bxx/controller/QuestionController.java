package com.bxx.controller;

import com.bxx.pojo.Question;
import com.bxx.service.QuestionService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	

	@GetMapping("/listQuestions")
	public PageUtil<Question> listQuestions(@RequestBody PageUtil<Question> pageUtil) {

		return questionService.listQuestions(pageUtil);
	}
	//新增用户
	@PostMapping("/insertQuestion")
	public R insertQuestion(@RequestBody Question question) {
		return questionService.insertQuestion(question);
	}
	//修改用户
	@PutMapping("/updateQuestion")
	public R updateQuestion(@RequestBody Question question) {
		return questionService.updateQuestion(question);
	}
	//删除用户
	@DeleteMapping("/deleteQuestion")
	public R deleteQuestion(@RequestBody String questionId) {
		System.out.println("questionId=="+questionId);
		return questionService.deleteQuestion(questionId);
	}


	/**
	 * 通过科目和试题类型名称获取条数
	 * @param subjectId
	 * @param questionTypeName
	 * @return
	 */
	@PostMapping("/getSubjectandNameCount")
	public  R getSubjectandNameCount(@RequestBody String subjectId,@RequestParam("questionTypeName") String questionTypeName){
		int subjectandNameCount = questionService.getSubjectandNameCount(subjectId, questionTypeName);


		return R.ok(subjectandNameCount,"ok");
	}


	

}
