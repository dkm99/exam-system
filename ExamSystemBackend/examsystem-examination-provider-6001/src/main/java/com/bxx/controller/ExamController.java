package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.service.ExamService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/exam")
public class ExamController {

    @Autowired
    private ExamService examService;


    @GetMapping("/listExams")
    public PageUtil<Exam> listExams(@RequestBody PageUtil<Exam> pageUtil) {

        return examService.listExams(pageUtil);
    }
    //新增考试
    @PostMapping("/insertExam")
    public R insertExam(@RequestBody Exam exam,@RequestParam("classIds") String classIds) {
        return examService.insertExam(exam,classIds);
    }
    //修改考试
    @PutMapping("/updateExam")
    public R updateExam(@RequestBody Exam exam) {
        return examService.updateExam(exam);
    }
    //取消考试
    @DeleteMapping("/cancelExam")
    public R cancelExam(@RequestBody String examId) {
        System.out.println("examId=="+examId);
        return examService.cancelExam(examId);
    }




}
