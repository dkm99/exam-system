package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.pojo.RedisVTO;
import com.bxx.pojo.Sequence;
import com.bxx.pojo.StudentExamAnswer;
import com.bxx.service.ExamService;
import com.bxx.service.StudentExamAnswerService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/studentExamAnswer")
public class StudentExamAnswerController {

    @Autowired
    private StudentExamAnswerService studentExamAnswerService;


    @GetMapping("/listStudentExamAnswers")
    public PageUtil<StudentExamAnswer> listStudentExamAnswers(@RequestBody PageUtil<StudentExamAnswer> pageUtil) {

        return studentExamAnswerService.listStudentExamAnswers(pageUtil);
    }

    //批改简答题分数
    @PutMapping("/updateStudentExamAnswer")
    public R updateStudentExamAnswer(@RequestBody StudentExamAnswer studentExamAnswer) {
        return studentExamAnswerService.updateStudentExamAnswer(studentExamAnswer);
    }

    @GetMapping("/test")
    public List<PageUtil<StudentExamAnswer>> test(@RequestBody PageUtil<StudentExamAnswer> pageUtil) {
        return studentExamAnswerService.test(pageUtil);
    }

    @PostMapping("/addStudentExamAnswerToRedis")
    public R addStudentExamAnswerToRedis(@RequestBody RedisVTO redisVTO) {

        return studentExamAnswerService.addStudentExamAnswerToRedis(redisVTO);
    }

    /**
     * 自动批改客观题
     * @param pageUtil
     * @return
     */
    @PostMapping("/automaticMarkingOfObjectiveQuestions")
    public R automaticMarkingOfObjectiveQuestions(@RequestBody PageUtil<StudentExamAnswer> pageUtil){

        return studentExamAnswerService.AutomaticMarkingOfObjectiveQuestions(pageUtil);
    }
}