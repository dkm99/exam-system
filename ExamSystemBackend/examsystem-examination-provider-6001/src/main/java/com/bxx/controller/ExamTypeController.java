package com.bxx.controller;

import com.bxx.pojo.ExamType;
import com.bxx.service.ExamTypeService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/examType")
public class ExamTypeController {
    @Autowired
    private ExamTypeService examTypeService;


    @GetMapping("/listExamTypes")
    public PageUtil<ExamType> listExamTypes(@RequestBody PageUtil<ExamType> pageUtil) {

        return examTypeService.listExamTypes(pageUtil);
    }
    //新增考场
    @PostMapping("/insertExamType")
    public R insertExamType(@RequestBody ExamType examType) {
        return examTypeService.insertExamType(examType);
    }
    //修改考场
    @PutMapping("/updateExamType")
    public R updateExamType(@RequestBody ExamType examType) {
        return examTypeService.updateExamType(examType);
    }
    //删除考场
    @DeleteMapping("/deleteExamType")
    public R deleteExamType(@RequestBody String examTypeId) {
        System.out.println("examTypeId=="+examTypeId);
        return examTypeService.deleteExamType(examTypeId);
    }




}
