package com.bxx.controller;

import com.bxx.pojo.ExamRoom;
import com.bxx.service.ExamRoomService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/examRoom")
public class ExamRoomController {
    @Autowired
    private ExamRoomService examRoomService;


    @GetMapping("/listExamRooms")
    public PageUtil<ExamRoom> listExamRooms(@RequestBody PageUtil<ExamRoom> pageUtil) {

        return examRoomService.listExamRooms(pageUtil);
    }
    //新增考场
    @PostMapping("/insertExamRoom")
    public R insertExamRoom(@RequestBody ExamRoom examRoom) {
        return examRoomService.insertExamRoom(examRoom);
    }
    //修改考场
    @PutMapping("/updateExamRoom")
    public R updateExamRoom(@RequestBody ExamRoom examRoom) {
        return examRoomService.updateExamRoom(examRoom);
    }
    //删除考场
    @DeleteMapping("/deleteExamRoom")
    public R deleteExamRoom(@RequestBody String examRoomId) {
        System.out.println("examRoomId=="+examRoomId);
        return examRoomService.deleteExamRoom(examRoomId);
    }




}
