package com.bxx.controller;

import com.bxx.pojo.Exam;
import com.bxx.pojo.ExamDTO;
import com.bxx.service.MyExamService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/exam")
public class MyExamController {

    //注入我的考试业务层
    @Autowired
    private MyExamService myExamService;

    /**
     * 根据用户id获取用户考试列表
     * @param userId
     * @return
     */
    @GetMapping("/getExamsByUserId")
    public PageUtil<Exam> getExamsByUserId(@RequestBody String userId){

        return myExamService.getExamsByUserId(userId);
    }

    /**
     * 开始考试
     * @param examId
     * @return
     */
    @GetMapping("/startExam")
    public R startExam(@RequestBody String examId, @RequestParam("userId") String userId){
        return myExamService.startExam(examId,userId);
    }

    //获取试题
    @GetMapping("/getTestQuestions")
    public R getTestQuestions(@RequestBody String examId){
        return myExamService.getTestQuestions(examId);
    }

    //提交考试
    @PostMapping("/submitExam")
    public R submitExam(@RequestBody ExamDTO examDTO){
        return myExamService.submitExam(examDTO);
    }
}
