package com.bxx.controller;

import com.bxx.pojo.Score;
import com.bxx.pojo.Sequence;
import com.bxx.service.ScoreService;
import com.bxx.service.SequenceService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: SubjectController
 * Descriptioen:
 * date: 2022/3/27 19:20
 * author :wfl
 */

@RestController
@RequestMapping("/score")
public class ScoreController {
    @Autowired
    private ScoreService scoreService;


    @GetMapping("/listScores")
    public PageUtil<Score> listScores(@RequestBody PageUtil<Score> pageUtil) {

        return scoreService.listScores(pageUtil);
    }
    @GetMapping("/Rewinding")
    public R Rewinding(@RequestBody Score score){
        return scoreService.Rewinding(score);
    }



}
