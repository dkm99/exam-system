package com.bxx.dao;

import com.bxx.pojo.Question;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface QuestionDao {
    /**
     * 通过科目查找试题集合
     * @param subjectId
     * @return
     */
    List<Question> getBySubjectIdQuestions(String subjectId);
}
