package com.bxx.dao;

import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.pojo.TestPaperQuestion;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: TestPaperDao
 * Descriptioen:
 * date: 2022/3/14 21:14
 * author :wfl
 */

@Mapper
@Repository //持久层注解
public interface TestPaperDao {
    /**
     * 多条件获取所有试卷
     * @return
     */
    List<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil);

    /**
     * 多条件获取所有试卷条数
     * @return
     */
   int listTestPapersCount(PageUtil<TestPaper> pageUtil);
    /**
     * 添加试卷
     * @param testPaper
     * @return
     */
    Integer insertTestPaper(TestPaper testPaper);

    /**
     * 修改试卷
     * @param testPaper
     * @return
     */
    Integer updateTestPaper(TestPaper testPaper);

    /**
     * 删除试卷
     * @param TestPaperId
     * @return
     */
    Integer deleteTestPaper(String TestPaperId);

    /**
     * 关联查询
     * @param pageUtil
     * @return
     */
    List<TestPaper> listTestPaperQuestions(PageUtil<TestPaper> pageUtil);
    /**
     * 向试卷中间表中添加关联数据
     * @param testPaperQuestion
     * @return
     */
    Integer insertTestPaperQuestion(TestPaperQuestion testPaperQuestion);


}
