package com.bxx.controller.controller;

import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.service.TestPaperService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/testPaper")
public class TestPaperController {

	@Autowired
	private TestPaperService testPaperService;

	@GetMapping("/listTestPapers")
	public PageUtil<TestPaper> listTestPaper(@RequestBody PageUtil<TestPaper> pageUtil) {

		return testPaperService.listTestPapers(pageUtil);
	}
	//新增试卷
	@PostMapping("/insertTestPaper")
	public R insertTestPaper(@RequestBody TestPaper testPaper,@RequestParam("ids") String ids) {
		System.out.println("wwwwwwwwwww="+testPaper.getTestPaperName());
		return testPaperService.insertTestPaper(testPaper,ids);
	}
	//修改试卷
	@PutMapping("/updateTestPaper")
	public R updateUser(@RequestBody TestPaper testPaper) {
		return testPaperService.updateTestPaper(testPaper);
	}
	//删除试卷
	@DeleteMapping("/deleteTestPaper")
	public R deleteTestPaper(@RequestBody String testPaperId) {
		return testPaperService.deleteTestPaper(testPaperId);
	}

	@GetMapping("/listTestPaperQuestions")
	public PageUtil<Question> listTestPaperQuestions(@RequestBody PageUtil<TestPaper> pageUtil) {
		return testPaperService.listTestPaperQuestions(pageUtil);


	}
	

}
