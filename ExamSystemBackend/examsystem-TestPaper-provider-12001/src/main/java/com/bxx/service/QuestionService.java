package com.bxx.service;

import com.bxx.util.R;

/**
 * ClassName: QuestionService
 * Descriptioen:
 * date: 2022/3/25 20:19
 * author :wfl
 */


public interface QuestionService {
    /**
     * 自动组卷
     * @param subjectId
     * @param testPaperId
     * @return
     */
    R  autoTestPaper(String subjectId,String testPaperId);
}
