package com.bxx.service;

import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.pojo.TestPaperQuestion;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

import java.util.List;

public interface TestPaperService {
    /**
     * 多条件获取所有试卷
     * @return
     */
    PageUtil<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil);


    /**
     * 添加试卷
     * @param testPaper
     * @return
     */
    R insertTestPaper(TestPaper testPaper,String ids);

    /**
     * 修改试卷
     * @param testPaper
     * @return
     */
    R updateTestPaper(TestPaper testPaper);

    /**
     * 删除试卷
     * @param testPaperId
     * @return
     */
    R deleteTestPaper(String testPaperId);
    PageUtil<Question> listTestPaperQuestions(PageUtil<TestPaper> pageUtil);
    /**
     * 自动组卷
     * @param subjectId
     * @param testPaperId
     * @return
     */
    R  autoTestPaper(String subjectId,String testPaperId);

}
