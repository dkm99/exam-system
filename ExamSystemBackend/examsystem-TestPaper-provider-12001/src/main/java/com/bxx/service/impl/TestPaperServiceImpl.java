package com.bxx.service.impl;

import com.bxx.dao.QuestionDao;
import com.bxx.dao.TestPaperDao;
import com.bxx.pojo.Question;
import com.bxx.pojo.TestPaper;
import com.bxx.pojo.TestPaperQuestion;
import com.bxx.service.TestPaperService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.ServiceMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * ClassName: ClassServiceImpl
 * Descriptioen:
 * date: 2022/2/13 20:08
 * author :wfl
 */

@Service
public class TestPaperServiceImpl implements TestPaperService {

    @Autowired
    private TestPaperDao testPaperDao;
    @Autowired
    private QuestionDao questionDao;
    @Override
    public PageUtil<TestPaper> listTestPapers(PageUtil<TestPaper> pageUtil) {
        List<TestPaper> testPapers = testPaperDao.listTestPapers(pageUtil);
        int listTestPapersCount = testPaperDao.listTestPapersCount(pageUtil);
        pageUtil.setData(testPapers);
        pageUtil.setCount(listTestPapersCount);
        return pageUtil;

    }



    @Override
    public R insertTestPaper(TestPaper testPaper,String ids) {
        System.out.println("questionId==>>"+ids);
        System.out.println("wwwwwwwwwww="+testPaper.getTestPaperName());
        //通过uuid生成考试id
        String testPaperId = UUID.randomUUID().toString();
        testPaper.setTestPaperId(testPaperId);
        Integer r = testPaperDao.insertTestPaper(testPaper);
        if(r>0){
            System.out.println("questionId==>>"+ids);
            String[] mid=ids.split(",");
            System.out.println("mid.length==>>"+mid.length);
            if(ids !=null && ids != ""&&!ids.equals("")) {
                for(int i =0;i<mid.length;i++) {
                    //同过遍历取出字符串里的每个id
                    String   id=mid[i];
                    TestPaperQuestion testPaperQuestion = new TestPaperQuestion();
                    testPaperQuestion.setTestPaperQuestionId(UUID.randomUUID().toString());
                    testPaperQuestion.setTestPaperId(testPaperId);
                    testPaperQuestion.setQuestionId(id);
                    testPaperDao.insertTestPaperQuestion(testPaperQuestion);
               }
            }

            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R updateTestPaper(TestPaper testPaper) {
        Integer r = testPaperDao.updateTestPaper(testPaper);

        if(r>0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Transactional //开启事务
    @Override
    public R deleteTestPaper(String testPaperId) {
        System.out.println("testPaperId===="+testPaperId);
        Integer r = testPaperDao.deleteTestPaper(testPaperId);
        if (r > 0) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }

    @Override
    public PageUtil<Question> listTestPaperQuestions(PageUtil<TestPaper> pageUtil) {
        List<TestPaper> testPapers = testPaperDao.listTestPaperQuestions(pageUtil);
        PageUtil<Question> pageUtil1=new PageUtil<Question>();
        List<Question> Questions=new ArrayList<>();
        if(testPapers!=null&testPapers.size()>0){
            for(int i = 0; i < testPapers.size(); i++){
                if(testPapers.get(0).getQuestions()!=null&testPapers.get(0).getQuestions().size()>0){
                    for(int j=0;j<testPapers.get(0).getQuestions().size();j++){
                        Questions = testPapers.get(0).getQuestions();

                    }
                }
            }
        }
        pageUtil1.setData(Questions);
        return  pageUtil1;
    }

    @Override
    public R autoTestPaper(String subjectId, String testPaperId) {
        //该科目下所有试题
        List<Question> listQuestion = questionDao.getBySubjectIdQuestions(subjectId);
        //乱序
        Collections.shuffle(listQuestion);
        List<Question> L1=new ArrayList<Question>() ;
        List<Question> L2=new ArrayList<Question>() ;
        List<Question> L3=new ArrayList<Question>() ;
        for(int i=0;i<listQuestion.size();i++){
            if(listQuestion.get(i).getQuestionTypeId()=="4fb62c55-7b07-4092-bb22-4f7c8669688a"){
                L1.add(listQuestion.get(i));
            }else{
                L2.add(listQuestion.get(i));
            }
        }



        return null;
    }


}
