package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * ClassName: OrganizationProvider_10001
 * Descriptioen:
 * date: 2022/2/13 17:18
 * author :wfl
 */

@SpringBootApplication
@EnableEurekaClient //在服务启动后自动注册到eureka中！
@EnableDiscoveryClient  //开启发现，可不写
public class TestPaperProvider_12001 {
    public static void main(String[] args) {
        SpringApplication.run(TestPaperProvider_12001.class,args);
    }
}
