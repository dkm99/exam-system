package com.bxx.controller;

import com.bxx.pojo.Subject;
import com.bxx.service.SubjectService;
import com.bxx.util.JsonUtil;
import com.bxx.util.LayuiJsonUtil;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/subject")
public class SubjectController {
    //注入业务层
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/list")
    public PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil){
        return subjectService.listSubjects(pageUtil);
    }

//    @GetMapping("/get/{subjectId}")
//    public R getSubject(@PathVariable("subjectId") String subjectId){
//        return subjectService.getSubject(subjectId);
//    }


    @PostMapping("/insertSubject")
    public R insertSubject(@RequestBody Subject subject){
        return subjectService.insertSubject(subject);
    }

    @DeleteMapping("/deleteSubject")
    public R deleteSubject(@RequestBody String subjectId){
        return subjectService.deleteSubject(subjectId);
    }

//    @PutMapping("/updateSubject")
//    public JsonUtil updateSubject(@RequestParam(value = "subject",required = false) Subject subject,@RequestBody Map<String,Object> map){
//        if(subject!=null){
//            System.out.println("provider.updateSubject.subject>"+subject.toString());
//        }
//        if(map!=null){
//            System.out.println("provider.updateSubject.map>"+map.toString());
//        }
//
//        return subjectService.updateSubject((Subject) map.get("subject"));
//    }

    @PutMapping("/updateSubject")
    public R updateSubject(@RequestBody Subject subject){

//        if(map!=null){
//            System.out.println("provider.updateSubject.map>"+map.toString());
//        }

        return subjectService.updateSubject(subject);
    }

}
