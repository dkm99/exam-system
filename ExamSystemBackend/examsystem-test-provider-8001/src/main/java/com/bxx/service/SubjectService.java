package com.bxx.service;

import com.bxx.pojo.Subject;
import com.bxx.util.JsonUtil;
import com.bxx.util.PageUtil;
import com.bxx.util.R;

import java.util.List;

public interface SubjectService {
    /**
     * 获取全部科目
     * @return 所有科目集合
     */
    PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil);

    /**
     * 根据id获取单个科目
     * @param subjectId
     * @return
     */
    //JsonUtil getSubject(String subjectId);

    /**
     * 新增科目
     * @param subject 新科目信息
     * @return
     */
    R insertSubject(Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    R deleteSubject(String subjectId);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    R updateSubject(Subject subject);
}
