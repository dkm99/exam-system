package com.bxx.service.impl;

import com.bxx.dao.SubjectDao;
import com.bxx.pojo.Subject;
import com.bxx.service.SubjectService;
import com.bxx.util.JsonUtil;
import com.bxx.util.LayuiJsonUtil;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service //业务层注解
public class SubjectServiceImpl implements SubjectService {

    //注入dao接口
    @Autowired
    private SubjectDao subjectDao;

    @Override
    public PageUtil<Subject> listSubjects(PageUtil<Subject> pageUtil) {

        List<Subject> subjects = subjectDao.listSubjects();

        //设置数据
        pageUtil.setTotalCount(subjects.size());
        pageUtil.setData(subjects);
        return pageUtil;
    }

//    @Override
//    public JsonUtil getSubject(String subjectId) {
//        JsonUtil jsonUtil=new JsonUtil();
//        Subject subject = subjectDao.getSubject(subjectId);
//        //设置数据
//        jsonUtil.setObject(subject);
//
//        return jsonUtil;
//    }

    @Override
    public R insertSubject(Subject subject) {
        JsonUtil jsonUtil=new JsonUtil();
        //生成id
        String id = UUID.randomUUID().toString();
        subject.setSubjectId(id);
        Integer i = subjectDao.insertSubject(subject);
        if(i>0){
            return R.ok("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Override
    public R deleteSubject(String subjectId) {
        JsonUtil jsonUtil=new JsonUtil();
        Integer i = subjectDao.deleteSubject(subjectId);
        if(i>0){
            return R.ok("删除成功");
        }else {
            return R.error("删除失败");
        }
    }

    @Override
    public R updateSubject(Subject subject) {
        JsonUtil jsonUtil=new JsonUtil();
        Integer i = subjectDao.updateSubject(subject);
        if(i>0){
            return R.ok("更新成功");
        }else {
            return R.error("更新失败");
        }
    }
}
