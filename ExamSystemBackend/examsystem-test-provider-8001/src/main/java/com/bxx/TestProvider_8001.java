package com.bxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient //在服务启动后自动注册到eureka中！
@EnableDiscoveryClient  //开启发现，可不写
public class TestProvider_8001 {
    public static void main(String[] args) {
        SpringApplication.run(TestProvider_8001.class,args);
    }
}
