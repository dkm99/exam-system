package com.bxx.dao;

import com.bxx.pojo.Subject;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface SubjectDao {

    /**
     * 获取全部科目
     * @return 所有科目集合
     */
    List<Subject> listSubjects();

    /**
     * 根据id获取单个科目
     * @param subjectId
     * @return
     */
    Subject getSubject(String subjectId);

    /**
     * 新增科目
     * @param subject 新科目信息
     * @return
     */
    Integer insertSubject(Subject subject);

    /**
     * 删除科目
     * @param subjectId
     * @return
     */
    Integer deleteSubject(String subjectId);

    /**
     * 修改科目
     * @param subject
     * @return
     */
    Integer updateSubject(Subject subject);
}
