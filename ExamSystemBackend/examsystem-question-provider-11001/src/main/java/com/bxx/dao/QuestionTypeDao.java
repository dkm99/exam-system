package com.bxx.dao;

import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface QuestionTypeDao {



    /**
     * 多条件获取所有题目类型
     * @return
     */
    List<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil);
//
//    /**
//     * 添加题目
//     * @param question
//     * @return
//     */
//    Integer insertQuestion(Question question);
//
//    /**
//     * 修改题目
//     * @param question
//     * @return
//     */
//    Integer updateQuestion(Question question);
//
//    /**
//     * 删除题目
//     * @param questionId
//     * @return
//     */
//    Integer deleteQuestion(String questionId);


}
