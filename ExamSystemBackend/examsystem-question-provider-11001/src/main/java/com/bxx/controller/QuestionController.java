package com.bxx.controller;

import com.bxx.pojo.Question;
import com.bxx.pojo.User;
import com.bxx.service.QuestionService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	

	@GetMapping("/listQuestions")
	public PageUtil<Question> listQuestions(@RequestBody PageUtil<Question> pageUtil) {

		return questionService.listQuestions(pageUtil);
	}
	//新增用户
	@PostMapping("/insertQuestion")
	public R insertQuestion(@RequestBody Question question) {
		return questionService.insertQuestion(question);
	}
	//修改用户
	@PutMapping("/updateQuestion")
	public R updateQuestion(@RequestBody Question question) {
		return questionService.updateQuestion(question);
	}
	//删除用户
	@DeleteMapping("/deleteQuestion")
	public R deleteQuestion(@RequestBody String questionId) {
		System.out.println("questionId=="+questionId);
		return questionService.deleteQuestion(questionId);
	}


	

}
