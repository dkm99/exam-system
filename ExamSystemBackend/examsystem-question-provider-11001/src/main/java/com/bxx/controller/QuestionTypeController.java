package com.bxx.controller;

import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.service.QuestionService;
import com.bxx.service.QuestionTypeService;
import com.bxx.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/questionType")
public class QuestionTypeController {

	@Autowired
	private QuestionTypeService questionTypeService;
	

	@GetMapping("/listQuestionTypes")
	public PageUtil<QuestionType> listQuestionTypes(@RequestBody PageUtil<QuestionType> pageUtil) {
		return questionTypeService.listQuestionTypes(pageUtil);
	}
	


	

}
