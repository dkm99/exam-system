package com.bxx.service;


import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.util.PageUtil;

public interface QuestionTypeService {

	/**
	 * 多条件获取所有题目类型
	 * @return
	 */
	PageUtil<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil);

	/**
	 * 添加题目类型
	 * @param questionType
	 * @return
	 */
	Integer insertQuestionType(QuestionType questionType);

	/**
	 * 修改题目类型
	 * @param questionType
	 * @return
	 */
	Integer updateQuestionType(QuestionType questionType);

	/**
	 * 删除题目类型
	 * @param questionTypeId
	 * @return
	 */
	Integer deleteQuestion(String questionTypeId);
}
