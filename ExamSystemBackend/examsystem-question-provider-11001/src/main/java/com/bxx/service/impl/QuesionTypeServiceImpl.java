package com.bxx.service.impl;

import com.bxx.dao.QuestionDao;
import com.bxx.dao.QuestionTypeDao;
import com.bxx.pojo.Question;
import com.bxx.pojo.QuestionType;
import com.bxx.service.QuestionService;
import com.bxx.service.QuestionTypeService;
import com.bxx.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuesionTypeServiceImpl implements QuestionTypeService {

	//用户持久层对象
	@Autowired
	private QuestionTypeDao questionTypeDao;

	@Override
	public PageUtil<QuestionType> listQuestionTypes(PageUtil<QuestionType> pageUtil) {
		List<QuestionType> questionTypes = questionTypeDao.listQuestionTypes(pageUtil);
		pageUtil.setData(questionTypes);
		return pageUtil;
	}

	@Override
	public Integer insertQuestionType(QuestionType questionType) {
		return null;
	}

	@Override
	public Integer updateQuestionType(QuestionType questionType) {
		return null;
	}

	@Override
	public Integer deleteQuestion(String questionTypeId) {
		return null;
	}
}
