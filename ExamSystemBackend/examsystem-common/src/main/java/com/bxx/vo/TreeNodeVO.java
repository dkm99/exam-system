package com.bxx.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

//树节点视图对象
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class TreeNodeVO {

    //节点id
    private Object id;
    //节点名称
    private String title;

    //节点路径
    private String url;
    //父节点编号
    private Integer parentId;
    //节点类型
    private String field;
    //子节点
    private List<TreeNodeVO> children;
    private boolean checked = false;
    private boolean disabled = false;
}
