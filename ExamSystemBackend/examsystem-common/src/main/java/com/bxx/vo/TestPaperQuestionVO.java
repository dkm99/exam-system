package com.bxx.vo;

import com.bxx.pojo.Question;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Timestamp;
import java.util.List;

//试卷题目视图对象
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class TestPaperQuestionVO {
    //private String testPaperQuestionId;//试卷题库id
    //private String testPaperId;//试卷id
    //private String questionId;//题目id

    private String questionTypeId;  //题目类型id
    private String questionTypeName;    //题目类型名称

    //题库
    private List<Question> questions;

}
