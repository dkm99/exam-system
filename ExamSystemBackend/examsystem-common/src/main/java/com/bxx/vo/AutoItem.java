package com.bxx.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * ClassName: AutoItem
 * Descriptioen:
 * date: 2022/3/31 15:59
 * author :wfl
 */


@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class AutoItem {
    String subjectId;
    int questionGrade;
    int totalScore;
    int choose;
    int completion;
    int judgment;
    int  schoolProfile;
}
