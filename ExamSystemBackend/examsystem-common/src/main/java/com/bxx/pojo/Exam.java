package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//考试
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Exam implements Serializable {
    private String examId;//考试id
    private String examName;//考试名称
    private Timestamp examStartTime;//考试开始时间
    private Timestamp examEndTime;//考试结束时间
    private Integer examPeopleNum;//考试人数
    private String effectiveStrength;//实际参与考试人数
    private Integer examStatus;//考试状态（0、未进行；1、进行中；2、已结束）
    private String testPaperId;//试卷id
    private String invigilateTeacherId;//监考老师id
    private String examTypeId;//考试类型id
    private String sequenceId;//批次id


}
