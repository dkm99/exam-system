package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//专业
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Major implements Serializable {
    private String majorId;//专业id
    private String majorName;//专业名称
    private Timestamp createTime;//创建时间
    private Timestamp updateTime;//更新时间
    private String departmentId;//所属院系id
}
