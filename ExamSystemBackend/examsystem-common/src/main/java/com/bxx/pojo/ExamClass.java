package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * ClassName: ExamClass
 * Descriptioen:
 * date: 2022/4/9 10:24
 * author :wfl
 */


//考试-班级
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class ExamClass implements Serializable {
  private String examId;
  private String classId;
  private Timestamp createTime;

}
