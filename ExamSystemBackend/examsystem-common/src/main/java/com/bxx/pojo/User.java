package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//用户
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class User implements Serializable {

    private String userId;//用户id
    private String userName;//用户名称
    private String userPassword;//用户密码
    private String headImg;//头像
    private String photo;//本人照片
    private Integer isLock;//是否锁定（0：否；1：是）
    private Timestamp updateTime;//更新时间
    private Timestamp createTime;//创建时间
    private Timestamp lastLoginTime;//最后登录时间
    private Integer pwdWrongCount;//密码错误次数
    private String lockTime;//锁定时间
    private String userEmail;//用户邮箱
    private String phoneNumber;//用户手机
    private String departmentId;//所属院系id
    private String classId;//所属班级id
    private String schoolId;//所属学校id

}
