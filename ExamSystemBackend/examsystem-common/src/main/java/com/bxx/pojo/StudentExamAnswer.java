package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//学生考试答案
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class StudentExamAnswer implements Serializable {
    private String studentExamAnswerId;//学生考试答案id
    private String studentId;//学生id
    private String answer;//当前题目学生答案
    private Double score;//当前题目得分
    private String testPaperQuestionId;//试卷题库id
    private TestPaperQuestion testPaperQuestion;  //试卷-试题
}
