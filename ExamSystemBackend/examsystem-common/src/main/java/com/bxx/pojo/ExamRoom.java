package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//考场
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class ExamRoom implements Serializable {
    private String examRoomId;//考场id
    private String examRoomNo;//考场号
    private String address;//地址
    private Integer status;//考场状态(0、空闲；1、占用）
    private String schoolId;//所属学校id

}
