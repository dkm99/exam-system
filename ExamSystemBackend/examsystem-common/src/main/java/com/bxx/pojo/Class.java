package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
//班级
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Class implements Serializable {
    private String classId;//班级id
    private String className;//班级名称
    private String counselorId;//辅导员id
    private String classYear;//届
    private Timestamp createTime;//创建时间
    private Timestamp updateTime;//更新时间
    private String majorId;//所属专业id
    private boolean selected=true;
    private boolean disabled=true;

}
