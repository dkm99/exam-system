package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//学校
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class School implements Serializable {
    private String schoolId;//学校id
    private String schoolName;//学校名称
    private String schoolType;//学校类型
    private String schoolProfile;//学校概况
    private Timestamp createTime;//创建时间

}
