package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//模块
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Module implements Serializable {
    private Integer moduleId;//模块id
    private String moduleName;//模块名称
    private Integer parentId;//父模块id
    private Integer moduleWeight;//模块权重
    private String moduleUrl;//模块链接
    private Timestamp createTime;//创建时间
    private Timestamp updateTime;//修改时间

}
