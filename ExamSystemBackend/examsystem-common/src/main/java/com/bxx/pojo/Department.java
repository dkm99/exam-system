package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//院系
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Department implements Serializable {
    private String departmentId;//院系id
    private String departmentName;//院系名称
    private String departmentProfile;//院系概况
    private Timestamp createTime;//创建时间
    private Timestamp updateTime;//更新时间
    private String schoolId;//所属学校id
    private String functionaryId;//院系负责人id

}
