package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//试卷-题库
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class TestPaperQuestion implements Serializable {
    private String testPaperQuestionId;//试卷题库id
    private String testPaperId;//试卷id
    private String questionId;//题目id
    private TestPaper testPaper; //试卷-试题多对多
    private Question question;//试题-试卷多对多

}
