package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * ClassName: ScoreDTO
 * Descriptioen:
 * date: 2022/4/12 19:16
 * author :wfl
 */

//试卷-成绩
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class ScoreDTO {
  private String studentId;
  private String questionTypeName;
}
