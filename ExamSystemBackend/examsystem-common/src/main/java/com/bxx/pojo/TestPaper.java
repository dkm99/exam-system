package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

//试卷
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class TestPaper implements Serializable {
    private String testPaperId;//试卷id
    private String testPaperName;//试卷名称
    private Integer status;//试卷状态（0、未发布；1、已发布）
    private Integer testPaperTime;//考试时长
    private String  testPaperNotes;
    private String subjectId;//科目id
    private List<Question>  questions;// 关系映射

}
