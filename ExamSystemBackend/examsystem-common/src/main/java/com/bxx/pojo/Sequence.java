package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//批次
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Sequence implements Serializable {
    private String sequenceId;//批次id
    private String addressSequence;//考场批次
    private String examSequence;//考试批次
    private Integer useTime;//使用时长（单位：分钟）
    private String examRoomId;//考场id

}
