package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//权限
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Permission implements Serializable {
    private String permissionId;//权限id
    private String permissionValue;//权限值
    private String permissionModule;//权限模块
    private String permissionName;//权限名称
    private Timestamp updateTime;//修改时间

}
