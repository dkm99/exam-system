package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//试卷-成绩
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Score implements Serializable {
    private String scoreId;//成绩id
    private Double score;//考试成绩
    private Integer scoreStatus;//状态（0、正常；1、缺考；2、作弊）
    private String testPaperId;//试卷id
    private String studentId;//学生id
    private String checkUserId;//改卷人id
    private StudentExam studentExam;//学生考试实体对象

}
