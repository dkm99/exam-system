package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

//用户传输对象
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class UserDTO implements Serializable {
    private String userId;//用户id
    private String userName;//用户名称
    private String userPassword;//用户密码
    private String headImg;//头像
    private String photo;//本人照片

    public String toJSONString(){
        return "{" +
                "\"userId\":\"" + userId +
                "\",\"userName\":\"" + userName +
                "\",\"userPassword\":\"" + userPassword +
                "\",\"headImg\":\"" + headImg +
                "\",\"photo\":\"" + photo +
                "\"}";
    }
}
