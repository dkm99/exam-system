package com.bxx.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

//题目
@Data //使用lombok生成get/set以及toString方法
@AllArgsConstructor //使用lombok生成全参构造方法
@NoArgsConstructor //使用lombok生成无参构造方法
@Accessors(chain = true)    //链式写法
public class Question implements Serializable {
    private String questionId;//题目id
    private String questionContent;//题目内容
    private String questionImg;//题目图片
    private String questionCreater;//出题者
    private Double questionScore;//题目分数
    private String questionGrade;//题型难度
    private Timestamp questionCreateTime;//题目创建时间
    private String itemA;//A项
    private String itemB;//B项
    private String itemC;//C项
    private String itemD;//D项
    private String itemE;//E项
    private String itemF;//F项
    private String questionAnswer;//正确答案
    private String questionAnalysis;//题目解析
    private String questionTypeId;//题目类型id
    private String subjectId;//科目id
    private Subject subject;
    private QuestionType questionType;

}
