package com.bxx.util;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 */
public class R extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    /**
     * 初始化对象
     */
    public R(){
        put("code", 0);
        put("msg", "success");
    }

    public static R error(){
        return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
    }

//    public static R error(String msg){
//        return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
//    }
    /**
     * 返回code码为1，自定义消息的对象
     * @param msg
     * @return
     */
    public static R error(String msg){
        return error(1, msg);
    }

    public static R error(int code, String msg){
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map){
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok(){
        return new R();
    }

    /**
     * 返回code码为0，自定义消息的对象
     * @param msg
     * @return
     */
    public static R ok(String msg){

        return ok(0, msg);
    }

    /**
     * 返回code码为0，带有数据的R对象
     * @param data
     * @return
     */
    public static R ok(Object data){

        return ok().put("data",data);
    }

    public static R ok(int code, String msg){
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public R put(String key, Object value){
        super.put(key, value);
        return this;
    }
}
