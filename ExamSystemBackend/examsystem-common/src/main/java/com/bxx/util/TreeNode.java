package com.bxx.util;

import java.util.List;

public class TreeNode {

	private Object id;
	private String title;
	private String path;
	private Integer parentid;
	private String field;
	private boolean checked=false;
	private boolean disabled=false;
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean getChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	private List<TreeNode> children;

	public TreeNode() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TreeNode(Integer id, String title, String path, Integer parentid, boolean checked, List<TreeNode> children) {
		super();
		this.id = id;
		this.title = title;
		this.path = path;
		this.parentid = parentid;
		this.checked = checked;
		this.children = children;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "TreeNode [id=" + id + ", title=" + title + ", path=" + path + ", parentid=" + parentid + ", checked="
				+ checked + ", disabled=" + disabled + ", children=" + children + "]";
	}

}
