package com.bxx.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 分页工具类
 */

public class PageUtil<T> implements Serializable{
    private static final long serialVersionUID = 1L;

    private int code = 0;
    //总记录数
    private int count = 0;

    //每页记录数
    private int limit = 10;

    //总页数
    private int totalPage;

    //当前页数
    private int page = 1;

    //列表数据
    private List<T> data;

    //泛型类型
    private T t;

    //条件集合
    private Map<String,Object> map;

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    /**
     * 分页
     * @param data        列表数据
     * @param count  总记录数
     * @param limit    每页记录数
     * @param page    当前页数
     */
    @Deprecated
    public PageUtil(List<T> data, int count, int limit, int page){
        this.data = data;
        this.count = count;
        this.limit = limit;
        this.page = page;
        this.totalPage = (int) Math.ceil((double)count/limit);
    }

    //分页
//    public PageUtil(List<T> data, int page, int limit){
//        if(page!=0){
//            page = page -1;
//        }
//    }
    public PageUtil() {
    }

    public PageUtil(int code, int count, int limit, int totalPage, int page, List<T> data, T t) {
        this.code = code;
        this.count = count;
        this.limit = limit;
        this.totalPage = totalPage;
        this.page = page;
        this.data = data;
        this.t = t;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "PageUtil{" +
                "code=" + code +
                ", count=" + count +
                ", limit=" + limit +
                ", totalPage=" + totalPage +
                ", page=" + page +
                ", data=" + data +
                ", t=" + t +
                '}';
    }
}
