package com.bxx.util;

public class JsonUtil {

	private Integer code;
	private String msg;
	private Object object;

	public JsonUtil() {
	}

	public JsonUtil(Integer code, String msg, Object object) {
		this.code = code;
		this.msg = msg;
		this.object = object;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		return "JsonUtil{" +
				"code=" + code +
				", msg='" + msg + '\'' +
				", object=" + object +
				'}';
	}
}
