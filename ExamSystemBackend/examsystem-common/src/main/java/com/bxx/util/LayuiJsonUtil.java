package com.bxx.util;

import java.util.List;

public class LayuiJsonUtil<T> {

    private Integer code = 0;
    private String msg="";
    private Long count;
    private List<T> data;

    private T t;
    private Integer page=0;
    private Integer limit=10;

    public LayuiJsonUtil() {
    }

    public LayuiJsonUtil(Integer code, String msg, Long count, List<T> data, T t, Integer page, Integer limit) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
        this.t = t;
        this.page = page;
        this.limit = limit;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public Integer getPage() {
        return page-1;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "LayuiJsonUtil{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                ", t=" + t +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }
}
