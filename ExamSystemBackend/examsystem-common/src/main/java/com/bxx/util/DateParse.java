package com.bxx.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParse {
    //创建日期格式化对象
    private static SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 字符串转换为日期格式
     * @param strDateTime 日期格式的字符串
     * @return Date类型的日期
     * @throws ParseException
     */
    public static Date parseTimeStamp(String strDateTime) throws ParseException {
        return sdf.parse(strDateTime);
    }

    /**
     * 将日期格式转换为字符串
     * @param date 日期
     * @return 日期格式的字符串
     */
    public static String parseString(Date date){
        return sdf.format(date);
    }

    /**
     * 将date类型的日期转换为Timestamp类型的日期
     * @param date date类型的日期
     * @return
     */
    public static Timestamp parseTimeStamp(Date date){
        return new Timestamp(date.getTime());
    }
}
