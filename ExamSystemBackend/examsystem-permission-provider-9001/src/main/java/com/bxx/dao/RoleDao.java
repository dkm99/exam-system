package com.bxx.dao;

import com.bxx.pojo.Role;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface RoleDao {

    /**
     * 获取全部角色
     * @param pageUtil
     * @return
     */
    List<Role> listRoles(PageUtil<Role> pageUtil);

    /**
     * 获取全部角色的数量
     * @param pageUtil
     * @return
     */
    Integer getCount(PageUtil<Role> pageUtil);

    /**
     * 添加角色
     * @param role
     * @return
     */
    Integer insertRole(Role role);

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    Integer deleteRole(String roleId);

    /**
     * 修改角色
     * @param role
     * @return
     */
    Integer updateRole(Role role);
}
