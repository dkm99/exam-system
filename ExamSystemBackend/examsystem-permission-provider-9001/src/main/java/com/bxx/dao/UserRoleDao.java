package com.bxx.dao;

import com.bxx.pojo.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface UserRoleDao {

    /**
     * 根据用户id获取用户拥有的角色
     * @param userId
     * @return
     */
    List<UserRole> getUserRoleByUserId(String userId);

    /**
     * 添加用户角色
     * @param userRole
     * @return
     */
    Integer insertUserRole(UserRole userRole);

    /**
     * 删除用户角色
     * @param userRole
     * @return
     */
    Integer deleteUserRole(UserRole userRole);
}
