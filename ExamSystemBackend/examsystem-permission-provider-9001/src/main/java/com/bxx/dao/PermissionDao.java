package com.bxx.dao;

import com.bxx.pojo.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface PermissionDao {

    /**
     * 获取全部权限
     * @return
     */
    List<Permission> listPermissions();

    /**
     * 根据权限id获取权限信息
     * @param permissionId
     * @return
     */
    Permission getPermissionByPermissionId(String permissionId);

    /**
     * 添加权限
     * @param permission
     * @return
     */
    Integer insertPermission(Permission permission);

    /**
     * 删除权限
     * @param permissionId
     * @return
     */
    Integer deletePermission(String permissionId);

    /**
     * 修改权限
     * @param permission
     * @return
     */
    Integer updatePermission(Permission permission);
}
