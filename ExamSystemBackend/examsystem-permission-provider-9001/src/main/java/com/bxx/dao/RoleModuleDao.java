package com.bxx.dao;

import com.bxx.pojo.Role;
import com.bxx.pojo.RoleModule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface RoleModuleDao {

    /**
     * 根据角色id获取角色拥有的模块
     * @param roleId
     * @return
     */
    List<RoleModule> getRoleModulesByRoleId(String roleId);

    /**
     * 根据角色id和模块id获取角色模块
     * @param roleModule
     * @return
     */
    RoleModule getRoleModuleByRoleIdAndModuleId(RoleModule roleModule);

    /**
     * 添加角色模块
     * @param roleModule
     * @return
     */
    Integer insertRoleModule(RoleModule roleModule);

    /**
     * 根据用户id和模块id删除角色模块
     * @param roleModule
     * @return
     */
    Integer deleteRoleModuleByRoleIdAndModuleId(RoleModule roleModule);

    /**
     * 根据角色id获取角色拥有的模块id集合
     * @param roleIdList 角色id集合
     * @return
     */
    List<Integer> getModuleIdByRoleIdList(@Param("roleIdList") List<String> roleIdList);
}
