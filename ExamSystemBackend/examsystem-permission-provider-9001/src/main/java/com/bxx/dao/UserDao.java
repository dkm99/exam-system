package com.bxx.dao;

import com.bxx.pojo.User;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface UserDao {

    /**
     * 登录
     * @param loginName
     * @return
     */
    User login(String loginName);

    /**
     * 多条件获取所有用户
     * @return
     */
    List<User> listUsers(PageUtil<User> pageUtil);

    /**
     *
     * @param pageUtil
     * @return
     */
    Integer getUsersCount(PageUtil<User> pageUtil);

    /**
     * 添加用户
     * @param user
     * @return
     */
    Integer insertUser(User user);

    /**
     * 修改用户
     * @param user
     * @return
     */
    Integer updateUser(User user);

    /**
     * 删除用户
     * @param userId
     * @return
     */
    Integer deleteUser(String userId);

    /**
     * 锁定或解锁用户
     * @param userId
     * @param isLockout
     * @return
     */
    //Integer locktopicUser(String userId,String isLockout);
    /**
     * 重置密码
     * @param userId
     * @return
     */
    //Integer resetPassword(String userId);



    /**
     * 获取用户的角色信息
     * @param id
     * @return
     */
    //List<Role> getRolesByUserId(String id);
    /**
     * 给用户设置角色
     * @return
     */
    //JsonUtil setRole(String id,String[] roleids);
    /**
     * 判断一个角色是否存在于集合中
     * @param role
     * @param roles
     * @return
     */
    //int existsRole(Roles role,List<Roles> roles);
    /**
     * 去除重复性数据
     * @param roles
     * @return
     */
    //List<Roles> distinctRoles(List<Roles> roles);
    /**
     * 根据角色获取权限
     * @param roleids
     * @return
     */
    //List<Permission> getPermissionsByUserRoles(List<String> roleids);

    /**
     * 根据用户id获取用户信息
     * @param userId
     * @return
     */
    User getUserById(String userId);
    /**
     * 不分页查询
     * @return
     */
    List<User> ListUser(PageUtil<User> pageUtil);
}
