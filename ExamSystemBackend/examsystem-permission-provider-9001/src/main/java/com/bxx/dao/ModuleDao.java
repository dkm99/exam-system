package com.bxx.dao;

import com.bxx.pojo.Module;
import com.bxx.util.PageUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface ModuleDao {
    /**
     * 获取全部模块信息
     * @return
     */
    List<Module> listModules();

    /**
     * 添加模块
     * @param module
     * @return
     */
    Integer insertModule(Module module);

    /**
     * 删除模块
     * @param moduleId
     * @return
     */
    Integer deleteModule(Integer moduleId);

    /**
     * 修改模块
     * @param module
     * @return
     */
    Integer updateModule(Module module);

    /**
     * 根据模块id获取模块信息
     * @param moduleId
     * @return
     */
    Module getModuleByModuleId(Integer moduleId);

    /**
     * 根据模块id集合获取模块
     * @param moduleIdList
     * @return
     */
    List<Module> getModulesByModuleIdList(@Param("moduleIdList")List<Integer> moduleIdList);
}
