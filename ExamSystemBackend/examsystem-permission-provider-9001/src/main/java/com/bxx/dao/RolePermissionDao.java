package com.bxx.dao;

import com.bxx.pojo.RolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository //持久层注解
public interface RolePermissionDao {
    /**
     * 根据角色id获取角色拥有的权限
     * @param roleId
     * @return
     */
    List<RolePermission> getRolePermissionsByRoleId(String roleId);

    /**
     * 根据角色id和权限id获取角色权限
     * @param rolePermission
     * @return
     */
    RolePermission getRolePermissionByRoleIdAndPermissionId(RolePermission rolePermission);

    /**
     * 添加角色权限
     * @param rolePermission
     * @return
     */
    Integer insertRolePermission(RolePermission rolePermission);

    /**
     * 根据角色id和权限id删除角色权限
     * @param rolePermission
     * @return
     */
    Integer deleteRolePermissionByRoleIdAndPermissionId(RolePermission rolePermission);
}
