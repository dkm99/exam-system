package com.bxx.service;

import com.bxx.pojo.RoleModule;
import com.bxx.util.R;

import java.util.List;

public interface RoleModuleService {
    /**
     * 根据角色id获取角色拥有的模块
     * @param roleId
     * @return
     */
    List<RoleModule> getRoleModulesByRoleId(String roleId);
    /**
     * 添加角色模块
     * @param roleModule
     * @return
     */
    Integer insertRoleModule(RoleModule roleModule);

    /**
     * 根据用户id和模块id删除角色模块
     * @param roleModule
     * @return
     */
    Integer deleteRoleModuleByRoleIdAndModuleId(RoleModule roleModule);
}
