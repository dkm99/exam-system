package com.bxx.service.impl;

import com.bxx.dao.RolePermissionDao;
import com.bxx.pojo.RolePermission;
import com.bxx.service.RolePermissionService;
import com.bxx.util.DateParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    //注入角色权限持久层
    @Autowired
    private RolePermissionDao rolePermissionDao;
    @Override
    public List<RolePermission> getRolePermissionsByRoleId(String roleId) {
        return rolePermissionDao.getRolePermissionsByRoleId(roleId);
    }

    @Override
    public Integer insertRolePermission(RolePermission rolePermission) {
        RolePermission rolePermission1 = rolePermissionDao.getRolePermissionByRoleIdAndPermissionId(rolePermission);

        //如果信息存在
        if(rolePermission1 != null){
            //不执行插入操作
            return 0;
        }
        rolePermission.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        return rolePermissionDao.insertRolePermission(rolePermission);
    }

    @Override
    public Integer deleteRolePermissionByRoleIdAndPermissionId(RolePermission rolePermission) {
        return rolePermissionDao.deleteRolePermissionByRoleIdAndPermissionId(rolePermission);
    }
}
