package com.bxx.service;


import com.bxx.pojo.LoginDTO;
import com.bxx.pojo.Module;
import com.bxx.pojo.User;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UserService {

	/**
	 * 登录
	 * @param loginDTO
	 * @return
	 */
	R login(LoginDTO loginDTO);

	/**
	 * 多条件获取所有用户
	 * @return
	 */
	PageUtil<User> listUsers(PageUtil<User> pageUtil);

	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	R insertUser(User user);

	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	R updateUser(User user);

	/**
	 * 删除用户
	 * @param userId
	 * @return
	 */
	R deleteUser(String userId);


	/**
	 * 重置密码
	 * @param userId
	 * @return
	 */
	R rePassword(String userId);

	/**
	 * 锁定或解锁
	 * @param user
	 * @return
	 */
	R lockOrUnlock(User user);

	/**
	 * 根据用户id获取用户的功能菜单树
	 * @param userId
	 * @return
	 */
	List<TreeNodeVO> getFunctionMenu(String userId);
	/**
	 * 不分页查询
	 * @return
	 */
	List<User> ListUser(PageUtil<User> pageUtil);

//	/**
//	 * 锁定或解锁用户
//	 * @param id
//	 * @param islockout
//	 * @return
//	 */
//	JsonUtil locktopicUser(String id,String islockout);
//	/**
//	 * 重置密码
//	 * @param id
//	 * @return
//	 */
//	JsonUtil resetPassword(String id);
//	/**
//	 * 获取用户的角色信息
//	 * @param id
//	 * @return
//	 */
//	List<Roles> getRolesByUserId(String id);
//	/**
//	 * 给用户设置角色
//	 * @return
//	 */
//	JsonUtil setRole(String id,String[] roleids);
//	/**
//	 * 判断一个角色是否存在于集合中
//	 * @param role
//	 * @param roles
//	 * @return
//	 */
//	int existsRole(Roles role,List<Roles> roles);
//	/**
//	 * 去除重复性数据
//	 * @param roles
//	 * @return
//	 */
//	List<Roles> distinctRoles(List<Roles> roles);
//	/**
//	 * 根据角色获取权限
//	 * @param roleids
//	 * @return
//	 */
//	List<Permission> getPermissionsByUserRoles(List<String> roleids);
}
