package com.bxx.service;

import com.bxx.pojo.Module;
import com.bxx.pojo.Permission;
import com.bxx.pojo.Role;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;

import java.util.List;

public interface RoleService {

    /**
     * 获取全部角色
     * @param pageUtil
     * @return
     */
    PageUtil<Role> listRoles(PageUtil<Role> pageUtil);

    /**
     * 添加角色
     * @param role
     * @return
     */
    R insertRole(Role role);

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    R deleteRole(String roleId);

    /**
     * 修改角色
     * @param role
     * @return
     */
    R updateRole(Role role);

    //==========================================
    /**
     * 获取角色拥有的模块和权限转换成树结构
     * @param roleId
     * @return
     */
    List<TreeNodeVO> getRoleHaveModuleAndPermissionConvertTree(String roleId);

    /**
     * 转换模块权限树
     * @param modules
     * @param permissions
     * @return
     */
    List<TreeNodeVO> convertModulePermissionTree(List<Module> modules, List<Permission> permissions);

    /**
     * 查找子节点
     * @param parent 父模块
     * @param modules 全部模块信息
     * @param permissions 全部权限信息
     * @return
     */
    TreeNodeVO findChildren(Module parent, List<Module> modules, List<Permission> permissions);

    //====================end====================

    /**
     * 根据角色拥有的模块和权限设置树节点选中
     * @param id
     * @param treeNodeVOs 全部模块和权限的树结构集合
     * @return
     */
    List<TreeNodeVO> setTreeNodeChecked(Object id, List<TreeNodeVO> treeNodeVOs);

    //================给角色设置模块和权限==============

    /**
     * 给角色设置模块和权限
     * @param roleId 角色id
     * @param moduleIdList 模块id集合
     * @param permissionIdList 权限id集合
     * @return
     */
    R giveRoleSetModulesAndPermissions(String roleId, List<Integer> moduleIdList, List<String> permissionIdList);

    /**
     * 给角色设置模块
     * 获取角色已经拥有的模块，和传来的模块id集合进行对比，添加和移除角色的模块
     * @param roleId
     * @param moduleIdList
     * @return
     */
    R giveRoleSetModules(String roleId, List<Integer> moduleIdList);
    /**
     * 给角色设置权限
     * 获取角色已经拥有的权限，和传来的权限id集合进行对比，添加和移除角色的权限
     * @param roleId
     * @param permissionIdList
     * @return
     */
    R giveRoleSetPermissions(String roleId, List<String> permissionIdList);
}
