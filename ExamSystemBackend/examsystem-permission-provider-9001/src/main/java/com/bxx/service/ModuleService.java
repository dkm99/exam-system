package com.bxx.service;

import com.bxx.pojo.Module;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;

import java.util.List;

public interface ModuleService {
    /**
     * 返回模块树
     * @return
     */
    List<TreeNodeVO> listModules();

    R findAllModules();

    /**
     * 添加模块
     * @param module
     * @return
     */
    R insertModule(Module module);

    /**
     * 删除模块
     * @param moduleId
     * @return
     */
    R deleteModule(Integer moduleId);

    /**
     * 修改模块
     * @param module
     * @return
     */
    R updateModule(Module module);

    /**
     * 转换为模块树
     * @param modules 要转换的模块数据
     * @return
     */
    List<TreeNodeVO> convertModuleTree(List<Module> modules);

    /**
     * 查找子节点
     * @param parent 父节点
     * @param modules 全部模块数据
     * @return
     */
    TreeNodeVO findChildren(TreeNodeVO parent,List<Module> modules);

    /**
     * 根据模块id获取模块信息
     * @param moduleId
     * @return
     */
    R getModuleByModuleId(Integer moduleId);

    //=========================
    /**
     * 根据角色获取角色的模块
     * @param roleId
     * @return
     */
    //List<TreeNodeVO> getRoleModule(String roleId);

    /**
     * 遍历模块树
     * @param roleId
     * @return
     */
    //TreeNodeVO traversalModuleTreeSetChecked(String roleId);
}
