package com.bxx.service;

import com.bxx.pojo.UserRole;
import com.bxx.util.R;

import java.util.List;

public interface UserRoleService {
    /**
     * 根据用户id获取用户拥有的角色
     * @param userId
     * @return
     */
    R getUserRoleByUserId(String userId);

    /**
     * 批量给用户添加角色
     * @param userId
     * @param roleIds
     * @return
     */
    R batchInsertUserRole(String userId, List<String> roleIds);

    /**
     * 批量移除用户的角色
     * @param userId
     * @param roleIds
     * @return
     */
    R batchDeleteUserRole(String userId, List<String> roleIds);
}
