package com.bxx.service;

import com.bxx.pojo.RolePermission;

import java.util.List;

public interface RolePermissionService {
    /**
     * 根据角色id获取角色拥有的权限
     * @param roleId
     * @return
     */
    List<RolePermission> getRolePermissionsByRoleId(String roleId);

    /**
     * 添加角色权限
     * @param rolePermission
     * @return
     */
    Integer insertRolePermission(RolePermission rolePermission);

    /**
     * 根据角色id和权限id删除角色权限
     * @param rolePermission
     * @return
     */
    Integer deleteRolePermissionByRoleIdAndPermissionId(RolePermission rolePermission);
}
