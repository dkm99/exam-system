package com.bxx.service.impl;

import com.bxx.dao.ModuleDao;
import com.bxx.pojo.Module;
import com.bxx.service.ModuleService;
import com.bxx.util.DateParse;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ModuleServiceImpl implements ModuleService {
    //模块持久层对象
    @Autowired
    private ModuleDao moduleDao;
    @Override
    public List<TreeNodeVO> listModules() {
        //获取全部模块信息
        List<Module> modules = moduleDao.listModules();
        //将模块集合转换为树结构
        List<TreeNodeVO> treeNodeVOs = convertModuleTree(modules);
        return treeNodeVOs;
    }

    @Override
    public R findAllModules() {
        List<Module> modules = moduleDao.listModules();
        if(modules != null){
            return R.ok().put("data",modules);
        }
        return R.error("模块信息获取失败！");
    }

    @Override
    public R insertModule(Module module) {
        module.setCreateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = moduleDao.insertModule(module);
        if(r > 0){
            return R.ok("添加成功");
        }else {
            return R.error("添加失败");
        }
    }

    @Override
    public R deleteModule(Integer moduleId) {
        Integer r = moduleDao.deleteModule(moduleId);
        if(r > 0){
            return R.ok("删除成功");
        }else {
            return R.error("删除失败");
        }
    }

    @Override
    public R updateModule(Module module) {
        module.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = moduleDao.updateModule(module);
        if(r > 0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }

    @Override
    public List<TreeNodeVO> convertModuleTree(List<Module> modules) {
        //创建树节点集合
        List<TreeNodeVO> treeNodeVOs = new ArrayList<TreeNodeVO>();
        //遍历集合
        for (Module module:modules) {
            if(module.getParentId() == 0){
                //创建树节点
                TreeNodeVO treeNodeVO = new TreeNodeVO();
                treeNodeVO.setId(module.getModuleId());
                treeNodeVO.setTitle(module.getModuleName());
                treeNodeVO.setParentId(module.getParentId());
                treeNodeVO.setUrl(module.getModuleUrl());
                //查找子集合，返回带有子集合的父模块
                treeNodeVO = findChildren(treeNodeVO, modules);
                //添加到树节点集合
                treeNodeVOs.add(treeNodeVO);
            }
        }
        return treeNodeVOs;
    }

    @Override
    public TreeNodeVO findChildren(TreeNodeVO parent,List<Module> modules) {

        //遍历所有模块
        for (Module module:modules) {
            //如果父节点的id等于子节点的父id则为该节点的子节点
            if(parent.getId() == module.getParentId()){
                //判断子节点集合是否为空
                if(parent.getChildren() == null){
                    //创建子节点集合
                    parent.setChildren(new ArrayList<TreeNodeVO>());
                }
                //创建子节点
                TreeNodeVO treeNodeVO = new TreeNodeVO();
                treeNodeVO.setId(module.getModuleId());
                treeNodeVO.setTitle(module.getModuleName());
                treeNodeVO.setParentId(module.getParentId());
                treeNodeVO.setUrl(module.getModuleUrl());
                //继续查找子集合，返回带有子集合的父模块
                treeNodeVO = findChildren(treeNodeVO, modules);
                //添加到子集合中
                parent.getChildren().add(treeNodeVO);
            }
        }
        return parent;
    }

    @Override
    public R getModuleByModuleId(Integer moduleId) {
        Module module = moduleDao.getModuleByModuleId(moduleId);
        if(module != null){
            return R.ok(module);
        }
        return R.error("获取数据异常");
    }

//    @Override
//    public List<TreeNodeVO> getRoleModule(String roleId) {
//        List<TreeNodeVO> treeNodeVOS = this.listModules();
//
//        return null;
//    }

}
