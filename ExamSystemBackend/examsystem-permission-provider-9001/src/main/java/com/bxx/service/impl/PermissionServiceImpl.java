package com.bxx.service.impl;

import com.bxx.dao.ModuleDao;
import com.bxx.dao.PermissionDao;
import com.bxx.dao.RoleModuleDao;
import com.bxx.dao.RolePermissionDao;
import com.bxx.pojo.Module;
import com.bxx.pojo.Permission;
import com.bxx.pojo.RoleModule;
import com.bxx.pojo.RolePermission;
import com.bxx.service.PermissionService;
import com.bxx.util.DateParse;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PermissionServiceImpl implements PermissionService {
    //注入权限持久层对象
    @Autowired
    private PermissionDao permissionDao;
    //注入模块持久层对象
    @Autowired
    private ModuleDao moduleDao;

    @Override
    public List<TreeNodeVO> listPermissions() {
        //获取全部模块信息
        List<Module> modules = moduleDao.listModules();
        //获取全部权限信息
        List<Permission> permissions = permissionDao.listPermissions();
        List<TreeNodeVO> treeNodeVOs = convertModulePermissionTree(modules, permissions);
        return treeNodeVOs;
    }

    @Override
    public R getPermissionByPermissionId(String permissionId) {
        Permission permission = permissionDao.getPermissionByPermissionId(permissionId);
        if (permission != null){
            return R.ok(permission);
        }
        return R.error("权限数据获取失败！");
    }

    //===============================
    @Override
    public List<TreeNodeVO> convertModulePermissionTree(List<Module> modules, List<Permission> permissions) {
        //创建树节点集合
        List<TreeNodeVO> treeNodeVOs = new ArrayList<TreeNodeVO>();
        //遍历集合
        for (Module module:modules) {
            if(module.getParentId() == 0){
                //创建树节点
                TreeNodeVO treeNodeVO = new TreeNodeVO();
                treeNodeVO.setId(module.getModuleId());
                treeNodeVO.setTitle(module.getModuleName());
                treeNodeVO.setParentId(module.getParentId());
                //设置节点类型
                treeNodeVO.setField("module");
                //禁用模块节点
                treeNodeVO.setDisabled(true);
                //查找子集合，返回带有子集合的树节点
                treeNodeVO = findChildren(treeNodeVO, modules, permissions);
                treeNodeVO.setTitle("【模块】" +treeNodeVO.getTitle());
                //添加到树节点集合
                treeNodeVOs.add(treeNodeVO);
            }
        }
        return treeNodeVOs;
    }

    @Override
    public TreeNodeVO findChildren(TreeNodeVO parent, List<Module> modules, List<Permission> permissions) {

        //查找子权限
        for (Permission permission:permissions) {
            //判断该权限是否属于该模块
            if((permission.getPermissionModule()).equals(parent.getTitle())){
                if(parent.getChildren() == null){
                    parent.setChildren(new ArrayList<TreeNodeVO>());
                }
                //创建树节点
                TreeNodeVO treeNodeVO = new TreeNodeVO();
                treeNodeVO.setId(permission.getPermissionId());
                treeNodeVO.setTitle("【权限】" + permission.getPermissionName());
                //设置节点类型
                treeNodeVO.setField("permission");
                //查找子集合，返回带有子集合的树节点
                //treeNodeVO = findChildren(treeNodeVO, modules, permissions);
                //添加到子节点中
                parent.getChildren().add(treeNodeVO);
            }
        }
        for (Module module:modules) {
            //判断是否是该模块的子模块
            if((module.getParentId()).equals(parent.getId())){
                if(parent.getChildren() == null){
                    parent.setChildren(new ArrayList<TreeNodeVO>());
                }
                //创建树节点
                TreeNodeVO treeNodeVO = new TreeNodeVO();
                treeNodeVO.setId(module.getModuleId());
                treeNodeVO.setTitle(module.getModuleName());
                treeNodeVO.setParentId(module.getParentId());
                //设置节点类型
                treeNodeVO.setField("module");
                //禁用模块节点
                treeNodeVO.setDisabled(true);
                //查找子集合，返回带有子集合的树节点
                treeNodeVO = findChildren(treeNodeVO, modules, permissions);
                treeNodeVO.setTitle("【模块】" +treeNodeVO.getTitle());
                //添加到树节点集合
                parent.getChildren().add(treeNodeVO);
            }
        }

        return parent;
    }

    //============================

    @Override
    public R insertPermission(Permission permission) {
        permission.setPermissionId(UUID.randomUUID().toString());
        Integer r = permissionDao.insertPermission(permission);
        if(r > 0){
            return R.ok("添加成功");
        }else {
            return R.error("添加失败");
        }
    }

    @Override
    public R deletePermission(String permissionId) {
        Integer r = permissionDao.deletePermission(permissionId);
        if(r > 0){
            return R.ok("删除成功");
        }else {
            return R.error("删除失败");
        }
    }

    @Override
    public R updatePermission(Permission permission) {
        permission.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        Integer r = permissionDao.updatePermission(permission);
        if(r > 0){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }
}
