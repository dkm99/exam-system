package com.bxx.service;

import com.bxx.pojo.Module;
import com.bxx.pojo.Permission;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;

import java.util.List;

public interface PermissionService {

    /**
     * 获取模块对应的权限树
     * @return
     */
    List<TreeNodeVO> listPermissions();

    /**
     * 根据权限id获取权限信息
     * @param permissionId
     * @return
     */
    R getPermissionByPermissionId(String permissionId);

    //==========转换模块权限树方法==========

    /**
     * 转换模块权限树
     * @param modules 全部模块信息
     * @param permissions 全部权限信息
     * @return
     */
    List<TreeNodeVO> convertModulePermissionTree(List<Module> modules, List<Permission> permissions);


    /**
     * 查找子节点，返回带子节点集合的树节点
     * @param parent 父节点
     * @param modules 全部模块信息
     * @param permissions 全部权限信息
     * @return
     */
    TreeNodeVO findChildren(TreeNodeVO parent, List<Module> modules, List<Permission> permissions);

    //===================

    /**
     * 添加权限
     * @param permission
     * @return
     */
    R insertPermission(Permission permission);

    /**
     * 删除权限
     * @param permissionId
     * @return
     */
    R deletePermission(String permissionId);

    /**
     * 修改权限
     * @param permission
     * @return
     */
    R updatePermission(Permission permission);
    //=============end=====================
}
