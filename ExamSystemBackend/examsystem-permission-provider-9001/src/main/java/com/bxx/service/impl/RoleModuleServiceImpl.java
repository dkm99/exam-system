package com.bxx.service.impl;

import com.bxx.dao.RoleModuleDao;
import com.bxx.pojo.RoleModule;
import com.bxx.service.RoleModuleService;
import com.bxx.util.DateParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RoleModuleServiceImpl implements RoleModuleService {

    //注入角色模块持久层
    @Autowired
    private RoleModuleDao roleModuleDao;

    //根据角色id获取角色拥有的模块
    @Override
    public List<RoleModule> getRoleModulesByRoleId(String roleId) {
        return roleModuleDao.getRoleModulesByRoleId(roleId);
    }

    @Override
    public Integer insertRoleModule(RoleModule roleModule) {
        //插入前查询角色模块信息是否存在
        RoleModule roleModule1 = roleModuleDao.getRoleModuleByRoleIdAndModuleId(roleModule);
        //如果信息存在
        if(roleModule1 != null){
            //不执行插入操作
            return 0;
        }
        roleModule.setUpdateTime(DateParse.parseTimeStamp(new Date()));
        return roleModuleDao.insertRoleModule(roleModule);

    }

    @Override
    public Integer deleteRoleModuleByRoleIdAndModuleId(RoleModule roleModule) {
        return roleModuleDao.deleteRoleModuleByRoleIdAndModuleId(roleModule);
    }
}
