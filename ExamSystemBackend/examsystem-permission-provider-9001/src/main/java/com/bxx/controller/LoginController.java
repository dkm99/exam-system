package com.bxx.controller;

import javax.servlet.http.HttpSession;

import com.bxx.pojo.LoginDTO;
import com.bxx.pojo.UserDTO;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.bxx.service.UserService;

@RestController
@RequestMapping("/user")
public class LoginController {

	@Autowired
	private UserService usersService;
	
	//登录
	@PostMapping("/login")
	public R login(@RequestBody LoginDTO loginDTO,HttpSession session) {
		R r = usersService.login(loginDTO);
		if((int)r.get("code") == 0){
			UserDTO user = (UserDTO) r.get("data");
			session.setAttribute("user",user);
			r.put("data",user.toJSONString());
			System.out.println("当前登录用户》" + session.getAttribute("user").toString());
		}
		return r;
	}
	
}
