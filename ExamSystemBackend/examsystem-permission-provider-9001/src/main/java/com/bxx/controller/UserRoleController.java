package com.bxx.controller;

import com.bxx.pojo.UserRole;
import com.bxx.service.UserRoleService;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userRole")
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;

    @GetMapping("/getUserRoleByUserId")
    public R getUserRoleByUserId(@RequestBody String userId){
        return userRoleService.getUserRoleByUserId(userId);
    }

    /**
     * 批量给用户添加角色
     * @param userId
     * @param roleIds
     * @return
     */
    @PostMapping("/batchInsertUserRole")
    public R batchInsertUserRole(@RequestParam("userId") String userId,@RequestBody List<String> roleIds){

        return userRoleService.batchInsertUserRole(userId,roleIds);
    }

    /**
     * 批量移除用户的角色
     * @param userId
     * @param roleIds
     * @return
     */
    @DeleteMapping("/batchDeleteUserRole")
    public R batchDeleteUserRole(@RequestParam("userId") String userId,@RequestBody List<String> roleIds){
        return userRoleService.batchDeleteUserRole(userId, roleIds);
    }
}
