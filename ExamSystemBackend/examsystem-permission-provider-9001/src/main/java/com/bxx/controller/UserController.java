package com.bxx.controller;

import com.bxx.pojo.User;
import com.bxx.service.UserService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	//根据该用户的角色获取功能菜单
//	@RequestMapping("/getFunctionMenu")
//	public List<Modules> getFunctionMenu(HttpServletRequest request) {
//		//获得前台传的一维数组
//		String[] roleids=request.getParameterValues("roleids[]");
//		//将一维数组转换为list
//		List<String> rids=new ArrayList<String>(Arrays.asList(roleids));
//		//根据用户角色获取所有功能模块
//		List<Modules> modules=rolesService.functionMenu(rids);
//		List<Modules> newmodules=modulesService.getTree(modules);
//		//转换为树结构
//		return newmodules;
//	}
	//根据该用户的角色获取权限编号
//	@RequestMapping("/getPermissionsByUserRoles")
//	public List<String> getPermissionsByUserRoles(HttpServletRequest request){
//		List<String> permissionids=new ArrayList<String>();
//		//获得前台传的一维数组
//		String[] roleids=request.getParameterValues("roleids[]");
//		//将一维数组转换为list
//		List<String> rids=new ArrayList<String>(Arrays.asList(roleids));
//		//根据角色id获取角色拥有的权限
//		List<Permission> permissions=usersService.getPermissionsByUserRoles(rids);
//		for (Permission permission : permissions) {
//			permissionids.add(permission.getPermissionid());
//		}
//		return permissionids;
//	}
	@GetMapping("/listUsers")
	public PageUtil<User> listUsers(@RequestBody PageUtil<User> pageUtil) {
		return userService.listUsers(pageUtil);
	}
	
	//新增用户
	@PostMapping("/insertUser")
	public R insertUser(@RequestBody User user) {
		return userService.insertUser(user);
	}
	//修改用户
	@PutMapping("/updateUser")
	public R updateUser(@RequestBody User user) {
		return userService.updateUser(user);
	}
	//删除用户
	@DeleteMapping("/deleteUser")
	public R deleteUser(@RequestBody String userId) {
		return userService.deleteUser(userId);
	}

	@PutMapping("/rePassword")
	public R rePassword(@RequestBody String userId){
		return userService.rePassword(userId);
	}

	@PutMapping("/lockOrUnlock")
	public R lockOrUnlock(@RequestBody User user){
		return userService.lockOrUnlock(user);
	}


	/**
	 * 根据用户id获取用户的功能菜单树
	 * @param userId
	 * @return
	 */
	@GetMapping("/getFunctionMenu")
	public List<TreeNodeVO> getFunctionMenu(@RequestBody String userId){
		return userService.getFunctionMenu(userId);
	}

	/**
	 * 不分页查询
	 * @return
	 */
	@GetMapping("/userList")
	public List<User>  userList(@RequestBody PageUtil<User> pageUtil){
		return  userService.ListUser(pageUtil);
	}
}
