package com.bxx.controller;

import com.bxx.pojo.Permission;
import com.bxx.service.PermissionService;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private PermissionService permissionService;
    /**
     * 获取模块对应的权限树
     * @return
     */
    @GetMapping("/listPermissions")
    public List<TreeNodeVO> listPermissions(){
        return permissionService.listPermissions();
    }

    @GetMapping("/getPermissionByPermissionId")
    public R getPermissionByPermissionId(@RequestBody String permissionId){
        return permissionService.getPermissionByPermissionId(permissionId);
    }


    /**
     * 添加权限
     * @param permission
     * @return
     */
    @PostMapping("/insertPermission")
    public R insertPermission(@RequestBody Permission permission){
        return permissionService.insertPermission(permission);
    }

    /**
     * 删除权限
     * @param permissionId
     * @return
     */
    @DeleteMapping("/deletePermission")
    public R deletePermission(@RequestBody String permissionId){
        return permissionService.deletePermission(permissionId);
    }

    /**
     * 修改权限
     * @param permission
     * @return
     */
    @PutMapping("/updatePermission")
    public R updatePermission(@RequestBody Permission permission){
        return permissionService.updatePermission(permission);
    }
}
