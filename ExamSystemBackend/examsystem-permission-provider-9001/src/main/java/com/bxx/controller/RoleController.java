package com.bxx.controller;

import com.bxx.pojo.Role;
import com.bxx.service.RoleService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 获取全部角色
     * @param pageUtil
     * @return
     */
    @GetMapping("/listRoles")
    public PageUtil<Role> listRoles(@RequestBody PageUtil<Role> pageUtil){
        return roleService.listRoles(pageUtil);
    }

    /**
     * 添加角色
     * @param role
     * @return
     */
    @PostMapping("/insertRole")
    public R insertRole(@RequestBody Role role){
        return roleService.insertRole(role);
    }

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    @DeleteMapping("/deleteRole")
    public R deleteRole(@RequestBody String roleId){
        return roleService.deleteRole(roleId);
    }

    /**
     * 修改角色
     * @param role
     * @return
     */
    @PutMapping("/updateRole")
    public R updateRole(@RequestBody Role role){
        return roleService.updateRole(role);
    }

    /**
     * 获取角色拥有的模块和权限转换成树结构
     * @param roleId
     * @return
     */
    @GetMapping("/getRoleHaveModuleAndPermissionConvertTree")
    public List<TreeNodeVO> getRoleHaveModuleAndPermissionConvertTree(@RequestBody String roleId){
        return roleService.getRoleHaveModuleAndPermissionConvertTree(roleId);
    }

    @PostMapping("/giveRoleSetModulesAndPermissions")
    public R giveRoleSetModulesAndPermissions(@RequestBody String roleId,@RequestParam("moduleIdList") List<Integer> moduleIdList,@RequestParam("permissionIdList") List<String> permissionIdList){

//        System.out.println("roleId>"+roleId);
//        System.out.println("moduleIds>" + moduleIdList.toString());
//        System.out.println("permissionIds>" +permissionIdList.toString());
//        return null;
        return roleService.giveRoleSetModulesAndPermissions(roleId,moduleIdList,permissionIdList);
    }
}
