package com.bxx.controller;

import com.bxx.pojo.Module;
import com.bxx.service.ModuleService;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/module")
public class ModuleController {

    //注入业务层对象
    @Autowired
    private ModuleService moduleService;

    @GetMapping("/listModules")
    public List<TreeNodeVO> listModules(){
        return moduleService.listModules();
    }
    @GetMapping("/getModuleByModuleId")
    public R getModuleByModuleId(@RequestBody Integer moduleId) {
        return moduleService.getModuleByModuleId(moduleId);
    }
    @PostMapping("/insertModule")
    public R insertModule(@RequestBody Module module){
        return moduleService.insertModule(module);
    }
    @DeleteMapping("/deleteModule")
    public R deleteModule(@RequestBody Integer moduleId){
        System.out.println("moduleId>"+ moduleId);
        return moduleService.deleteModule(moduleId);
    }
    @PutMapping("/updateModule")
    public R updateModule(@RequestBody Module module){
        return moduleService.updateModule(module);
    }
    @GetMapping("/findAllModules")
    public R findAllModules(){
        return moduleService.findAllModules();
    }
}
