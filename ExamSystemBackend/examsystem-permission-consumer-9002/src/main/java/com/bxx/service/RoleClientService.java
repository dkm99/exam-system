package com.bxx.service;

import com.bxx.pojo.Role;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-PERMISSION-PROVIDER")
@RequestMapping("/role")
public interface RoleClientService {

    /**
     * 获取全部角色
     * @param pageUtil
     * @return
     */
    @GetMapping("/listRoles")
    PageUtil<Role> listRoles(@RequestBody PageUtil<Role> pageUtil);

    /**
     * 添加角色
     * @param role
     * @return
     */
    @PostMapping("/insertRole")
    R insertRole(@RequestBody Role role);

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    @DeleteMapping("/deleteRole")
    R deleteRole(@RequestBody String roleId);

    /**
     * 修改角色
     * @param role
     * @return
     */
    @PutMapping("/updateRole")
    R updateRole(@RequestBody Role role);

    @GetMapping("/getRoleHaveModuleAndPermissionConvertTree")
    List<TreeNodeVO> getRoleHaveModuleAndPermissionConvertTree(@RequestBody String roleId);

    @PostMapping("/giveRoleSetModulesAndPermissions")
    R giveRoleSetModulesAndPermissions(@RequestBody String roleId,@RequestParam("moduleIdList") List<Integer> moduleIdList,@RequestParam("permissionIdList") List<String> permissionIdList);
}
