package com.bxx.service;

import com.bxx.pojo.Module;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-PERMISSION-PROVIDER")
@RequestMapping("/module")
public interface ModuleClientService {

    @GetMapping("/listModules")
    List<TreeNodeVO> listModules();

    @GetMapping("/getModuleByModuleId")
    R getModuleByModuleId(@RequestBody Integer moduleId);

    @PostMapping("/insertModule")
    R insertModule(@RequestBody Module module);

    @DeleteMapping("/deleteModule")
    R deleteModule(@RequestBody Integer moduleId);

    @PutMapping("/updateModule")
    R updateModule(@RequestBody Module module);

    @GetMapping("/findAllModules")
    R findAllModules();
}
