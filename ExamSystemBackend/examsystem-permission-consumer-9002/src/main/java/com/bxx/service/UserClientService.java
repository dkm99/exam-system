package com.bxx.service;

import com.bxx.pojo.LoginDTO;
import com.bxx.pojo.User;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-PERMISSION-PROVIDER")
@RequestMapping("/user")
public interface UserClientService {

    /**
     * 登录
     * @param loginDTO
     * @return
     */
    @PostMapping("/login")
    R login(@RequestBody LoginDTO loginDTO);

    /**
     * 多条件获取所有用户
     * @return
     */
    @GetMapping("/listUsers")
    PageUtil<User> listUsers(@RequestBody PageUtil<User> pageUtil);

    /**
     * 添加用户
     * @param user
     * @return
     */
    @PostMapping("/insertUser")
    R insertUser(@RequestBody User user);

    /**
     * 修改用户
     * @param user
     * @return
     */
    @PutMapping("/updateUser")
    R updateUser(@RequestBody User user);

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @DeleteMapping("/deleteUser")
    R deleteUser(@RequestBody String userId);

    @PutMapping("/rePassword")
    R rePassword(@RequestBody String userId);

    @PutMapping("/lockOrUnlock")
    R lockOrUnlock(@RequestBody User user);

    /**
     * 根据用户id获取用户的功能菜单树
     * @param userId
     * @return
     */
    @GetMapping("/getFunctionMenu")
    List<TreeNodeVO> getFunctionMenu(@RequestBody String userId);
    /**
     * 不分页查询
     * @return
     */
    @GetMapping("/userList")
    public List<User>  userList(@RequestBody PageUtil<User> pageUtil);
}
