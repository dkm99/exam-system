package com.bxx.service;

import com.bxx.pojo.Permission;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-PERMISSION-PROVIDER")
@RequestMapping("/permission")
public interface PermissionClientService {
    /**
     * 获取模块对应的权限树
     * @return
     */
    @GetMapping("/listPermissions")
    List<TreeNodeVO> listPermissions();

    @GetMapping("/getPermissionByPermissionId")
    R getPermissionByPermissionId(@RequestBody String permissionId);

    /**
     * 添加权限
     * @param permission
     * @return
     */
    @PostMapping("/insertPermission")
    R insertPermission(@RequestBody Permission permission);

    /**
     * 删除权限
     * @param permissionId
     * @return
     */
    @DeleteMapping("/deletePermission")
    R deletePermission(@RequestBody String permissionId);

    /**
     * 修改权限
     * @param permission
     * @return
     */
    @PutMapping("/updatePermission")
    R updatePermission(@RequestBody Permission permission);
}
