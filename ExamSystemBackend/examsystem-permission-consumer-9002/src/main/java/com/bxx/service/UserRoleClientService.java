package com.bxx.service;

import com.bxx.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(value = "EXAMSYSTEM-PERMISSION-PROVIDER")
@RequestMapping("/userRole")
public interface UserRoleClientService {

    @GetMapping("/getUserRoleByUserId")
    R getUserRoleByUserId(@RequestBody String userId);

    /**
     * 批量给用户添加角色
     * @param userId
     * @param roleIds
     * @return
     */
    @PostMapping("/batchInsertUserRole")
    R batchInsertUserRole(@RequestParam("userId") String userId,@RequestBody List<String> roleIds);

    /**
     * 批量移除用户的角色
     * @param userId
     * @param roleIds
     * @return
     */
    @DeleteMapping("/batchDeleteUserRole")
    R batchDeleteUserRole(@RequestParam("userId") String userId,@RequestBody List<String> roleIds);
}
