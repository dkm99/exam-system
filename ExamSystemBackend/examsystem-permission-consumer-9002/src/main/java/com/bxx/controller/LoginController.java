package com.bxx.controller;

import com.bxx.pojo.LoginDTO;
import com.bxx.service.UserClientService;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/consumer")
public class LoginController {
    @Autowired
    private UserClientService userClientService;

    //登录
    @PostMapping("/login")
    public R login(LoginDTO loginDTO) {

        //System.out.println("登录提交用户信息>"+loginDTO.toString());
        return userClientService.login(loginDTO);
    }
    //退出登录
    @GetMapping("/logout")
    public void logout(HttpSession session) {
        //清除session中用户的信息
        session.removeAttribute("user");
    }
}
