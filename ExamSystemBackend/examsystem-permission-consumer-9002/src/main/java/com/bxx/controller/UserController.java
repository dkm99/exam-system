package com.bxx.controller;

import com.bxx.pojo.Module;
import com.bxx.pojo.User;
import com.bxx.service.UserClientService;
import com.bxx.util.PageUtil;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/consumer/user")
public class UserController {

    @Autowired
    private UserClientService userClientService;

    @GetMapping("/listUsers")
    public PageUtil<User> listUsers(PageUtil<User> pageUtil,User user,String beginDate,String endDate) {
        Map<String,Object> map = new HashMap<String, Object>();
        if (user != null){
            map.put("user", user);
        }
        if(beginDate != null && beginDate != ""){
            map.put("beginDate", beginDate);
        }
        if(endDate != null && endDate != ""){
            map.put("endDate", endDate);
        }
        pageUtil.setMap(map);
        return userClientService.listUsers(pageUtil);
    }

    //新增用户
    @PostMapping("/insertUser")
    public R insertUser(User user) {
        return userClientService.insertUser(user);
    }
    //修改用户
    @PutMapping("/updateUser")
    public R updateUser(User user) {
        System.out.println("consumer.updateUser>"+user);
        return userClientService.updateUser(user);
    }
    //删除用户
    @DeleteMapping("/deleteUser")
    public R deleteUser(String userId) {
        return userClientService.deleteUser(userId);
    }


    //重置用户密码
    @PutMapping("/rePassword")
    public R rePassword(String userId){
        return userClientService.rePassword(userId);
    }

    @PutMapping("/lockOrUnlock")
    public R lockOrUnlock(String userId, Integer isLock){
        User user = new User();
        user.setUserId(userId);
        //isLock取值必须为：0:解锁，1:锁定
        user.setIsLock(isLock);

        return userClientService.lockOrUnlock(user);
    }

    /**
     * 根据用户id获取用户的功能菜单树
     * @param userId
     * @return
     */
    @GetMapping("/getFunctionMenu")
    List<TreeNodeVO> getFunctionMenu(String userId){

        return userClientService.getFunctionMenu(userId);
    }
    /**
     * 不分页查询
     * @return
     */
    @GetMapping("/userList")
    public List<User>  userList(PageUtil<User> pageUtil){


        return userClientService.userList(pageUtil);
    }

}
