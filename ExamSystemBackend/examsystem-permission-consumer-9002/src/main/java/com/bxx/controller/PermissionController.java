package com.bxx.controller;

import com.bxx.pojo.Permission;
import com.bxx.service.PermissionClientService;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/consumer/permission")
public class PermissionController {

    @Autowired
    private PermissionClientService permissionClientService;

    @GetMapping("/listPermissions")
    public List<TreeNodeVO> listPermissions(){
        return permissionClientService.listPermissions();
    }

    @GetMapping("/getPermissionByPermissionId")
    public R getPermissionByPermissionId(String permissionId){
        return permissionClientService.getPermissionByPermissionId(permissionId);
    }

    /**
     * 添加权限
     * @param permission
     * @return
     */
    @PostMapping("/insertPermission")
    public R insertPermission(Permission permission){
        return permissionClientService.insertPermission(permission);
    }

    /**
     * 删除权限
     * @param permissionId
     * @return
     */
    @DeleteMapping("/deletePermission")
    public R deletePermission(String permissionId){
        return permissionClientService.deletePermission(permissionId);
    }

    /**
     * 修改权限
     * @param permission
     * @return
     */
    @PutMapping("/updatePermission")
    public R updatePermission(Permission permission){
        return permissionClientService.updatePermission(permission);
    }
}
