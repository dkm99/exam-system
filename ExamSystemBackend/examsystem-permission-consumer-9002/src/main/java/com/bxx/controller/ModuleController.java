package com.bxx.controller;

import com.bxx.pojo.Module;
import com.bxx.service.ModuleClientService;
import com.bxx.util.R;
import com.bxx.vo.TreeNodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/consumer/module")
public class ModuleController {

    //注入模块feign对象
    @Autowired
    private ModuleClientService moduleClientService;

    @GetMapping("/listModules")
    public List<TreeNodeVO> listModules(){
        return moduleClientService.listModules();
    }

    @GetMapping("/getModuleByModuleId")
    public R getModuleByModuleId(Integer moduleId){
        return moduleClientService.getModuleByModuleId(moduleId);
    }
    @PostMapping("/insertModule")
    public R insertModule(Module module){
        return moduleClientService.insertModule(module);
    }
    @DeleteMapping("/deleteModule")
    public R deleteModule(Integer moduleId){
        System.out.println("moduleId>"+ moduleId);
        return moduleClientService.deleteModule(moduleId);
    }
    @PutMapping("/updateModule")
    public R updateModule(Module module){
        return moduleClientService.updateModule(module);
    }

    @GetMapping("/findAllModules")
    public R findAllModules(){
        return moduleClientService.findAllModules();
    }
}
