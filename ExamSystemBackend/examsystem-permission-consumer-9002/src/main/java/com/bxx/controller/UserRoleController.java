package com.bxx.controller;

import com.bxx.service.UserRoleClientService;
import com.bxx.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/consumer/userRole")
public class UserRoleController {

    @Autowired
    private UserRoleClientService userRoleClientService;

    @GetMapping("/getUserRoleByUserId")
    public R getUserRoleByUserId(String userId){
        return userRoleClientService.getUserRoleByUserId(userId);
    }

    /**
     * 批量给用户添加角色
     * @param userId
     * @return
     */
    @PostMapping("/batchInsertUserRole")
    public R batchInsertUserRole(String userId, HttpServletRequest request){

        //获取角色id
        String[] roleIds = request.getParameterValues("roleIds[]");
        List<String> roleIdArr = Arrays.asList(roleIds);

        return userRoleClientService.batchInsertUserRole(userId,roleIdArr);
    }

    /**
     * 批量移除用户的角色
     * @param userId
     * @return
     */
    @DeleteMapping("/batchDeleteUserRole")
    public R batchDeleteUserRole(String userId, HttpServletRequest request){
        String[] roleIds = request.getParameterValues("roleIds[]");
        List<String> roleIdArr = Arrays.asList(roleIds);

        return userRoleClientService.batchDeleteUserRole(userId, roleIdArr);
    }
}
