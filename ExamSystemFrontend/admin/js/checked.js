layui.use(['form'],function(){
	var form=layui.form
	//自定义验证
				form.verify({
					ph:[/^1[3,5,7,8]\d{9}$/, "请输入13,15,17,18开头的手机号"],
	                uname:[/^[\S]{3,}$/, "登录名不少于3个字符"],
	                upwd:[/^[\S]{5,}$/, "密码名不少于5个字符"]
				});
});
