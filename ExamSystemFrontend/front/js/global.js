var globalData={
	permissionServer:"http://127.0.0.1:9002/consumer",
	organizationServer:"http://127.0.0.1:10002/consumer",
	examinationServer:"http://127.0.0.1:6002/consumer",
	//questionServer:"http://127.0.0.1:11002/consumer",
	//pre:"http://127.0.0.1:8020/权限管理系统/",
	//myToken:"b611c568-a4da-44b3-8bb9-fae06c04f679",
	setUserInfo:function(userId,userName){
		sessionStorage.setItem("userId",userId);
		sessionStorage.setItem("userName",userName);
		//sessionStorage.setItem("roleids",roleids);
		//sessionStorage.setItem("token",token);

	},
	getCurUserId:function(){
		return sessionStorage.getItem("userId");
	},
	getCurUserName:function(){
		return sessionStorage.getItem("userName");
	},
	setIds: function(ids) {
		window.localStorage.setItem("ids", ids);
	},
	getIds: function() {
		return window.localStorage.getItem("ids");
	},
	setExamId:function(examId){
		sessionStorage.setItem("examId",examId);
	},
	getExamId(){
		return sessionStorage.getItem("examId");
	},
	// getCurUid:function(){
	// 	return sessionStorage.getItem("uid");
	// },
	// getCurUName:function(){
	// 	return sessionStorage.getItem("uname");
	// },
	// getCurRoleids:function(){
	// 	var rs=sessionStorage.getItem("roleids");
	// 	var arr=rs.split(",");
	// 	var data="[";
	// 	for(var i=0;i<arr.length;i++){
	// 		arr[i]="\'"+arr[i]+"\'";
	// 	}
	// 	return eval("["+arr.join()+"]");
	// },
	// getCurToken:function(){
	// 	return sessionStorage.getItem("token");
	// },
	// setCurUserPermissions:function(permissions){
	// 	sessionStorage.setItem("permissions",permissions);
	// },
	// getCurUserPermissions:function(){
	// 	return sessionStorage.getItem("permissions");
	// },
	// setCurPageModuleId:function(moduleId){
	// 	sessionStorage.setItem("pageModuleId",moduleId);
	// },
	// getCurPageModuleId:function(){
	// 	return sessionStorage.getItem("pageModuleId");
	// },
	logout:function(){
		sessionStorage.clear();//清除所有sessionStorage存储内容
		localStorage.clear();//清除所有localStorage存储内容
	}
};
